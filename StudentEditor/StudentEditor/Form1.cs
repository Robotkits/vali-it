﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace StudentEditor
{
    public partial class Student_Editor : Form
    {
        public Student_Editor()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string firstName = textBoxFirstName.Text;
            string lastName = textBoxLastName.Text;
            int age = Convert.ToInt32(textBoxAge.Text);



            string connectionString = "Server = DESKTOP-1R06THO; Database = AspNetCore; Trusted_Connection = True";
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            string sql =
                "INSERT INTO Student " +
                "(FirstName, LastName, Age) " +
                "VALUES " +
                "(@FirstName, @LastName, @Age )";

            SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("@FirstName", firstName);
            command.Parameters.AddWithValue("@LastName", lastName);
            command.Parameters.AddWithValue("@Age", age);

            int linesChanged = command.ExecuteNonQuery();
            MessageBox.Show("Changes count: " + linesChanged);

            connection.Close();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBoxAge_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
