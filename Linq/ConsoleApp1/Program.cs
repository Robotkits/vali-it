﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = new List<Student>()
            {
                new Student()
                {
                    FirstName = "Kalle",
                    LastName = "Talle",
                    Age = 20
                },
                 new Student()
                {
                    FirstName = "talle",
                    LastName = "kalle",
                    Age = 40
                },
                  new Student()
                {
                    FirstName = "Malle",
                    LastName = "Palle",
                    Age = 30
                },
                   new Student()
                {
                    FirstName = "Jalle",
                    LastName = "Ialle",
                    Age = 50
                },
                    new Student()
                {
                    FirstName = "Valle",
                    LastName = "Salle",
                    Age = 10
                },
                     new Student()
                {
                    FirstName = "Oalle",
                    LastName = "Xalle",
                    Age = 20
                }
            };


            // SELECT * FROM Student
            var allStudents = (from student in students
                              select student
                              ).ToList();

            // Lambda süntaks

            allStudents = students.ToList();

            // SELECT * FROM Student
            // WHERE Age > 20
            // ORDER BY FirstName DESC

            // QUERY süntaks
            var oldStudents = (from student in students
                               where student.Age > 20
                               orderby student.Age ascending,
                               student.FirstName descending
                               select student).ToList();

            // Lambda süntaks

            oldStudents = students
                .Where(x => x.Age > 20)
                .OrderBy(x => x.Age)
                .ThenByDescending(x => x.FirstName)
                .ToList();

            var youngStudents = new List<Student>();
            
            foreach (var student in students)
            {
                if (student.Age < 20)
                {
                    youngStudents.Add(student);
                }
            }

            // Prindi mulle kõikide kasutajate nime initsiaalid
            // Täienda nii, et ainult nendel õpilastel, kellel on eesnimes 5 tähte
            var initials = (from student in students
                           where student.FirstName.Length == 5
                           select student.FirstName.Substring(0, 1) + " " + student.LastName.Substring(0, 1)
                           ).ToList();

            initials = students
                .Where(x => x.FirstName.Length == 5)
                .Select(x => x.FirstName.Substring(0, 1) + " " + x.LastName.Substring(0, 1))
                .ToList();

            // Täisarvude masiivis anna mulle kõik täisarvud
            int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

            var evenNumbers = (from number in numbers
                            where number % 2 == 0
                            select number)
                            .ToList();

            evenNumbers = numbers
                .Where(x => x % 2 == 0)
                .ToList();

            // Anna mulle õpilaste nimekiri, kus perekonnanimes kõik a,e,i on asendatud 0-ga
            var changedStudens = (from student in students
                                  select student.LastName
                                  .Replace("a", "o")
                                  .Replace("e", "o")
                                  .Replace("i", "o")
                          ).ToList();

            changedStudens = students
                .Select(x => x.LastName
                .Replace("a", "o")
                .Replace("e", "o")
                .Replace("i", "o"))
                .ToList();

            var changedStudensTwo = (from student in students
                                  select 
                                  new Student
                                  {
                                      FirstName = student.FirstName,
                                      LastName = student.LastName == null ? student.LastName
                                        .Replace("a", "o")
                                        .Replace("e", "o")
                                        .Replace("i", "o") : null,
                                    Age = student.Age
                                  }).ToList();

            changedStudensTwo = students
                .Select(x => new Student()
                {
                    FirstName = x.FirstName,
                    LastName = x.LastName == null ? x.LastName
                                    .Replace("a", "o")
                                    .Replace("e", "o")
                                    .Replace("i", "o") : null,
                    Age = x.Age
                }).ToList();


            int a = 4;
            int b;

            if (a < 10)
            {
                b = 1;
            }
            else
            {
                b = 2;
            }

            b = a < 10 ? 1 : 2; // if teistmoodi kirjutatult.

            string text;
            if(students[2].LastName == null)
            {
                text = "tühi!";
            }
            else
            {
                text = "tühi";
            }

            text = students[2].LastName ?? "tühi";

            // Anna mulle kasutaja/õpilane, Jalle Ialle
            var oneStudent = students
                .Where(x => x.FirstName == "Jalle" && x.LastName == "Ialle")
                .FirstOrDefault();

            // First ja FirstOrDefault vahe on see, et First annab exceptioni,
            // kui sellist tulemust ei leitud, aga FirstOrDefault tagastab null või (int,double puhul 0. bool puhul false)

            // Maksimum vanus
            var max = students.Max(x => x.Age);
            // Anna mulle vanim õpilane
            var twoStudent = students
                .Where(x => x.Age == students.Max(y => y.Age))
                .FirstOrDefault();

            twoStudent = students.OrderByDescending(x => x.Age)
                .FirstOrDefault();

            // Anna mulle õpilaste keskmine vanus

            var avgAge = students.Average(x => x.Age);

            Console.ReadLine();
        }
    }
}
