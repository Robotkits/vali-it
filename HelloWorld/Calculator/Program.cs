﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {

        // muuda korrutamise ja liitmise meetodid nii, et toimivad ka 3 numbriga
        // Loo meetod, mis liidab kõik masiivi numbrid kokku

        static void Main(string[] args)
        {
            // küsi kasutajalt arvud, millega tahad tehteid teha
            // eralda arvud tühikuga( küsi ühe reaga)

            Console.WriteLine("Sisesta arvud, millega soovid tehteid teha!\nEralda need arvud tühikuga.\nJätkamiseks vajuta ENTER");

            string[] elements = Console.ReadLine().Split(' ');

            int[] numbers = new int[elements.Length];

            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = Convert.ToInt32(elements[i]);
            }

            for (int i = 0; i < numbers.Length; i++) // kontroll
            {
                Console.WriteLine(numbers[i]);
            }

            // olenevalt sellest, kas kasutaja sisestas 2, 3 või enam arvu
            // paku kasutajale tehted, mida kasutaja saab nendega teha

            //Console.WriteLine("Vali kas sa soovid:\na) Liita\nb) Lahutada\nc) Korrutada\nd) Jagada");
            //string choseOperation = Console.ReadLine().Replace(')', ' ').ToLower();

            //Console.WriteLine(choseOperation);//kontroll

            //Console.ReadLine();
            if (numbers.Length == 1)
            {
                Console.WriteLine("Tehte jaoks on vaja vähemalt 2 numbrit");
            }

            else if (numbers.Length == 2)
            {
                Console.WriteLine(
                    "Vali kas sa soovid:" +
                    "\na) Liita" +
                    "\nb) Lahutada" +
                    "\nc) Korrutada" +
                    "\nd) Jagada");
                string choseOperation = Console.ReadLine().Replace(')', ' ').ToLower();
                if (choseOperation == "a")
                {
                    Console.WriteLine($"Arvude {numbers[0]} ja {numbers[1]} summa on {Sum(numbers[0], numbers[1])}");
                    Console.ReadLine();
                }
                else if (choseOperation == "b")
                {
                    Console.WriteLine($"Arvude {numbers[0]} ja {numbers[1]} vahe on {Subtract(numbers[0], numbers[1])}");
                    Console.ReadLine();
                }
                else if (choseOperation == "c")
                {
                    Console.WriteLine($"Arvude {numbers[0]} ja {numbers[1]} korrutis on {Multiply(numbers[0], numbers[1])}");
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine($"Arvude {numbers[0]} ja {numbers[1]} jagatis on {Divide(numbers[0], numbers[1])}");
                    Console.ReadLine();
                }
            }

            else if (numbers.Length == 3)
            {
                Console.WriteLine("Vali kas sa soovid:\na) Liita\nb) Korruta");
                string choseOperation = Console.ReadLine().Replace(')', ' ').ToLower();
                Console.ReadLine();
            }

            else
            {
                Console.WriteLine("a) Liida");
                string answer = Console.ReadLine();
                if (answer == "a")
                {
                    // Arvude 3, 4, 64, -23, 33, 200 ja -17 summa on 352362 // alljärgnev näide, kuidas listiga viimane nr maha võtta
                    //List<int> numbersList = numbers.ToList();
                    //numbersList.RemoveAt(numbersList.Count - 1);

                    Console.WriteLine($"ARVUDE {string.Join(", ", numbers)} on {Sum(numbers)}");
                    Console.ReadLine();
                }
            }
        }
                // kui panna 2 arvu
                // Vali tehe
                // a) Liida
                // b) Lahuta
                // c) Korruta
                // d) Jaga


                //    // liitmine
                //    int sum = Sum(2, 6);
                //    Console.WriteLine($"Arvude 2 ja 6 summa on {sum}");
                //    Console.WriteLine($"Arvude 2, 6 ja 10 summa on {Sum(2, 6, 10)}");
                //    Console.ReadLine();


                //    // lahutamine Subtract
                //    int subtract = Subtract(2, 6);
                //    Console.WriteLine($"Arvude 2 ja 6 vahe on {subtract}");
                //    Console.ReadLine();


                //    // korrutamine Multiply
                //    int multiply = Multiply(2, 6);
                //    Console.WriteLine($"Arvude 2 ja 6 korrutis on {multiply}");
                //    Console.WriteLine($"Arvude 2, 6 ja 10 korrutis on {Multiply(2, 6, 10)}");
                //    Console.ReadLine();

                //    // jagamine Divide
                //    double divide = Divide(2, 6);
                //    Console.WriteLine($"Arvude 2 ja 6 jagatis on {divide}");
                //    Console.ReadLine();

                //    // masiiiv
                //    Console.WriteLine("Masiivi arvude summa on {0}", Sum(new int[] { 1, 2, 3, 4, 5, 6 });
                //    Console.ReadLine();

                // Sum
            static int Sum(int a, int b, int c = 0)
            {
                int sum = a + b + c;
                return sum;
            }

            // SUbtract
            static int Subtract(int a, int b)
            {
                int subtract = a - b;
                return subtract;
            }

            // Multiply
            static int Multiply(int a, int b, int c = 1)
            {
                int multiply = a * b * c;
                return multiply;
            }

            // Divide
            static double Divide(double a, double b)
            {
                double divide = a / b;
                return divide;
            }

            // Loo meetod, mis liidab kõik masiivi numbrid kokku

            static int Sum(int[] numbers)
            {
                int sum = 0;
                for (int i = 0; i < numbers.Length; i++)
                {
                    sum = sum + numbers[i];
                    // sum += numbers[i]
                }
                return sum;
                }
            }
    
}
