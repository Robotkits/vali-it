﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintHello();
            PrintHello();
            PrintHello();

            PrintText("Tere, kuidas elad?", 2);
            PrintText("Elan hästi!", 4);

            PrintHello(3);

            PrintText("TE<rERER>R", 4, true);

            PrintText("Tervist", 4);
            // booleani väärtuse true (oneLine), kasutab if funktsiooni ja prindib ühele reale (Write())
            // booleani väärtuse false puhul, kasutab else tsüklit ja prindib üksteise alla (WriteLine())

            Console.ReadLine();
        }

        static void PrintHello()
        {
            Console.WriteLine("Hello");
        }

        static void PrintText(string text)
        {
            Console.WriteLine(text);
        }

        static void PrintHello(int howManyTimes)
        {
            for (int i = 0; i < howManyTimes; i++)
            {
                PrintHello();
            }
        }

        //// Meetod PrintText, kus lisaks tekstile on parameeter, mis ütleb mitu korda printida

        //static void PrintText(string text, int howManyTimes)
        //{
        //    for (int i = 0; i < howManyTimes; i++)
        //    {
        //        Console.WriteLine(text);
        //    }
        //}
            // Veel üks meetod PrintText,
            // mis lisaks tekstile ja korduste arvule omab kolmandat parameetrit, mis määrab ära,
            //kas need korduvad tekstid prinditakse ühele või mitmele reale

        static void PrintText(string text, int howManyTimes, bool oneLine = false)
        {
                if (oneLine)
                {
                    for (int i = 0; i < howManyTimes; i++)
                    {
                        Console.Write(text);
                    }
                }
                else
                {
                    for (int i = 0; i < howManyTimes; i++)
                    {
                        Console.WriteLine(text);
                    }   
                }

                // alljärgnev näide, kui kõigile on antud väärtused, text, howManyTimes ja bool, vaikseväärtuseid saab panna alati paremalt vasakule
            //static void PrintText(string text = "Karvane", int howManyTimes = 1, bool oneLine = false)
            //{
            //    if (oneLine)
            //    {
            //        for (int i = 0; i < howManyTimes; i++)
            //        {
            //            Console.Write(text);
            //        }
            //    }
            //    else
            //    {
            //        for (int i = 0; i < howManyTimes; i++)
            //        {
            //            Console.WriteLine(text);
            //        }
            //    }
            }
    }
}
