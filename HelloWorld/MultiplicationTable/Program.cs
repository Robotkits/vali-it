﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiplicationTable
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(" For Multiplication table, push ENTER");
            Console.ReadLine();
            
            // iga rida
            for(int i = 1; i < 11; i++)
            {
                // Rea element reas
                for (int j = 1; j < 11; j++)
                {
                    Console.Write("{0,5}", j * i);
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
