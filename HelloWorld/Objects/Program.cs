﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    class Program
    {
        static void Main(string[] args)
        {
            // Loon klassist Monitor ühe eksemplari nimega monitor
            // Keela kasutajal diagonaali seadistamine, luba ainult küsida
            // Kui kasutaja seadistab ekraani suuruse, siis seadista automaatselt selle järgi ka diagonaal
            Monitor lgMonitor = new Monitor
            {
                Manufacturer = "LG",
                Color = Color.Black,
                ScreenSize = ScreenSize.Large,
                ScreenType = ScreenType.OLED,
                Resolution = "1920x1080 FULL HD"
            };
            Console.WriteLine($"Monitori tootja on {lgMonitor.Manufacturer}," +
                $" suurus on {lgMonitor.ScreenSize}," +
                $"diagonaal on {lgMonitor.Diagonal}," +
                $" ekraanitüüp on {lgMonitor.ScreenType}," +
                $" värv on {lgMonitor.Color} ja resolutsioon on {lgMonitor.Resolution}.");

            int diagonal = (int)ScreenSize.Large;

            //visualstudio poolt lihtsustatud variandid
            Monitor hpMonitor = new Monitor
            {
                Manufacturer = "HP",
                Color = Color.Green,
                ScreenSize = ScreenSize.Medium,
                ScreenType = ScreenType.AMOLED,
                Resolution = "1920x1080 FULL HD"
            };
            Console.WriteLine($"Monitori tootja on {hpMonitor.Manufacturer}," +
                $" suurus on {hpMonitor.ScreenSize}," +
                $"diagonaal on {hpMonitor.Diagonal}," +
                $" ekraanitüüp on {hpMonitor.ScreenType}," +
                $" värv on {hpMonitor.Color} ja resolutsioon on {hpMonitor.Resolution}.");

            Monitor dellMonitor = new Monitor
            {
                Manufacturer = "DELL",
                Color = Color.Red,
                ScreenSize = ScreenSize.Small,
                ScreenType = ScreenType.LCD,
                Resolution = "1920x1080 FULL HD"
            };
            Console.WriteLine($"Monitori tootja on {dellMonitor.Manufacturer}," +
                $" suurus on {dellMonitor.ScreenSize}," +
                $"diagonaal on {dellMonitor.Diagonal}," +
                $" ekraanitüüp on {dellMonitor.ScreenType}," +
                $" värv on {dellMonitor.Color} ja resolutsioon on {dellMonitor.Resolution}.");
            Console.ReadLine();

            // tee masiiv kolmest monitorist
            // for tsükkel, mis prindib kõigi nende monitoride tootjad,mille diagonaali on suurem kui 22
            Monitor[] monitors = new Monitor[] {lgMonitor, hpMonitor, dellMonitor};
            //monitors[0] = monitor;
            //monitors[1] = hpMonitor;
            //monitors[2] = dellMonitor;

            // Prindi kõigi kolme monitoride värvid
            for (int i = 0; i < monitors.Length; i++)
            {
                Console.WriteLine(monitors[i].Color);
            }

            Console.WriteLine();

            foreach (Monitor monitor in monitors)
            {
                Console.WriteLine(monitor.Color);
            }

            Console.WriteLine();

            foreach (Monitor monitor in monitors)
            {
                if (monitor.Diagonal > 22)
                {
                    Console.WriteLine($"Suurem kui 22\" on {monitor.Manufacturer}");
                }
            }
            Console.ReadLine();
        }
    }
}
