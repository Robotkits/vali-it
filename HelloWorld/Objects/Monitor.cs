﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Objects
{
    // enum on tüüp, kus saab defineerida erinevaid valiku
    // Kasutatakse siis, kui valikud ei muutu programmi töö jooksul
    // Tegelikult salvestatakse enum'id alati int'na
    enum ScreenType
    {
        LCD, TFT, OLED, AMOLED
    }
    enum Color
    {
        Red = 1, Green = 2, Blue = 3, Grey = 4, Black = 5, White = 6
    }
    enum ScreenSize
    {
        Large = 27, Medium = 22, Small = 17
    }

    class Monitor
    {
        private Color color;
        // privaat muutujad
        // inglise keeles kutsutakse seda field (väli) 
        private string manufacturer;
        private string resolution;
        private double diagonal;
        ScreenType screentype;
        ScreenSize screensize;



        public ScreenType ScreenType
        {
            get
            {
                return screentype;
            }
            set
            {
                screentype = value;
            }
        }
        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }
        public ScreenSize ScreenSize
        {
            get
            {
                return screensize;
            }
            set
            {
                diagonal = (int)value;
                screensize = value;
            }
        }
        //public string Manufacturer { get => manufacturer; set => manufacturer = value; }
        // Property 
        // get ja set meetodeid nimetatakse accessors
        public string Manufacturer
        {
            get
            {
                if (manufacturer == "Tootja puudub")
                {
                    Console.WriteLine("Kahjuks on sellel monitoril tootja seadistamata");
                    Console.WriteLine("Palun kirjuta tootja:");
                    manufacturer = Console.ReadLine();
                }
                return manufacturer;
            }
            set
            {
                if (value == " Huawei")
                {
                    Console.WriteLine("Sellise tootja monitore meil pole");
                    manufacturer = "Tootja puudub";
                }
                else
                {
                    manufacturer = value;
                }
                // value tähendab seda väärtust, mis ma parasjagu panen property väärtuseks
                manufacturer = value;
            }
        }
        public string Resolution
        {
            get
            {
                return resolution;
            }
            set
            {
                resolution = value;
            }
        }
        public double Diagonal
        {
            get
            {
                return diagonal;
            }
            //    //set //Keela kasutajal diagonaali seadistamine, luba ainult küsida
            //    //{
            //    //    diagonal = value;
            //    //}
            //    //}
            //    // Kapseldamine, tähendab, et klass kontrollib millistele muutujatele
            //    // lubab väljaspoolt ligi pääeseda ( ligi pääsevad teised klassid)
            //    // Encapsulation
        }
    }
}
