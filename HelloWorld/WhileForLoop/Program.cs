﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileForLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            // Prindi ekraanile arvud 1 kuni 5
            int n = 1;
            while(n < 6)
            {
             Console.WriteLine(n);
                n++;
                Console.ReadLine();
            }

            // Küsi kasutajalt mis päev täna on seni kuni ta ära arvab /näited while ja for

            //string weekDay = "neljapäev";
            //string answer = "";

            //while (weekDay != answer.ToLower())
            //{
            //    Console.WriteLine("Mis nädalapäev meil täna on?");
            //    answer = Console.ReadLine();
            //}
            string answer = "";

            for(;"neljapäev" != answer.ToLower(); )
            {
                Console.WriteLine("Mis nädalapäev meil täna on?");
                answer = Console.ReadLine();
            }
            Console.ReadLine();
        }
    }
}
