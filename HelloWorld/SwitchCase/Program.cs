﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchCase
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("a");
            Console.WriteLine("b");
            Console.WriteLine("c");
            Console.WriteLine("d");
            Console.WriteLine("e");
            string choseOperation = Console.ReadLine();


            if (choseOperation == "a" || choseOperation == "A" || choseOperation == "1" || choseOperation == "1" || choseOperation == "-1")
            {
                Console.WriteLine(1);
            }
            if (choseOperation == "b" || choseOperation == "B")
            {
                Console.WriteLine(2);
            }
            if (choseOperation == "c")
            {
                Console.WriteLine(3);
            }
            if (choseOperation == "d")
            {
                Console.WriteLine(3);
            }
            else
            {
                Console.WriteLine(5);
            }

            // Switch'i algus


            switch (choseOperation)
            {
                case "a":
                case "A":
                case "1":
                case "-1":
                    Console.WriteLine(1);
                    break;
                case "b":
                case "B":
                    Console.WriteLine(2);
                    break;
                case "c":
                    Console.WriteLine(3);
                    break;
                case "d":
                    Console.WriteLine(4);
                    break;
                case "e":
                    Console.WriteLine(5);
                    break;
                default:
                    break;

            }
            Console.ReadLine();
        }
    }
}
