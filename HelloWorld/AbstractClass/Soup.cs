﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClass
{
    // Abstract meetodi ülekirjutamine
    class Soup : Food
    {
        public override void GoOff()
        {
            Console.WriteLine("Supp läks halvaks");
        }

        public override int GetCalories()
        {
            //return calories / 2;
            return base.GetCalories() / 2;
        }

    }
}
