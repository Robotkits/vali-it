﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClass
{
    class Program
    {
        static void Main(string[] args)
        {
            Food food = new Soup();
            Console.WriteLine("Supis on {0} kalorit", food.GetCalories());

            food.GoOff();
            Console.ReadLine();
        }
    }
}
