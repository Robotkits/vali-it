﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strings
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = "Elas ";

            text = text + "metsas ";
            text = text + "Mutionu!";

            Console.WriteLine(text);

            Console.WriteLine();

            string word1 = "Kes";
            string word2 = "elab";
            string word3 = "metsa";
            string word4 = "sees";

            Console.WriteLine(word1 + " " + word2 + " " + word3 + " " + word4 + "?");
            // Teksti formaatimine, kus määrame kohahoidjad indekisitega, alates 0'st ja hiljem asendame muutujatega
            // eraldades muutujad komaga samas jäjekorras, mis indeksid
            Console.WriteLine("{0} {1} {2} {3}?", word1, word2, word3, word4);

            string nationality = "eestlane";
            string gender = "mees";
            string location = "Tallinn";

            //Ma olen meessoost, elan Talllinnas ja muidu olen eestlane ning olen hea mees.
            Console.WriteLine("Ma olen " + gender + "soost, elan " + location
                + "as ja muidu olen " + nationality + " ning olen hea " + gender + ".");

            Console.WriteLine("Ma olen {0}soost, elan {1}as ja muidu olen {2} ning olen hea {0}.",
                gender, location, nationality);

            Console.WriteLine();

            //On mõistlik nii kirjutada, kui tekst on pikem ja muutujaid on rohkem,parem ülevaade, see võtab ka vähem mälu

            // string.Format kasutab vähem mälu, kui + märgiga stringide liitmine
            // Console.WriteLine() kasutab sisemiselt string.Format() funktsiooni
            string sentence = string.Format("Ma olen {0}soost, elan {1}as ja muidu olen {2} ning olen hea {0}.",
                gender, location, nationality);

            Console.WriteLine(sentence);

            //string.Format sisemiselt kasutab stringide liitmiseks StringBuilderit
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(word1);
            stringBuilder.Append(" ");
            stringBuilder.Append(word2);
            stringBuilder.Append(" ");
            stringBuilder.Append(word3);
            sentence = stringBuilder.ToString();
            Console.WriteLine(sentence);

            Console.WriteLine(stringBuilder.GetType());

            Console.ReadLine();
        }
    }
}
