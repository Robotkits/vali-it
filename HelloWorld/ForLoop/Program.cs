﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            // For tsükkel on selline tsükkel, kus korduste arv on teada

            // for( ; ; ) siin on kolm parameetrit
            //for(; ; )
            //{
            //    Console.WriteLine("Hello");
            //}
            // 1)    int i = 0; =>
            //       siin saab luua muutujaid ja neid algväärtustada
            //       luuakse täisarv i, mille väärtus hakkab tsükli sees muutuma
            // 2)   i < 3 => tingimus, mis peab olema tõene, et tsükkel käivituks ja korduks
            // 3)   i++ => tegevus, mida ma tahan iga tsükli korduse lõpus korrata
            //      i++ on sama, mis i = i + 1 ehk iseenda suurendamine 1 võrra

            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Õppisin");

            }
            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine(i);
            }

            // Prindi arvud 50 kuni 100

            for (int i = 50; i <= 100; i++)
            {
                Console.WriteLine(i);
            }

            //Prindi arvud -20 kuni 10ni ja 20st 40ni

            for (int i = -20; i <= 40; i++)
            {
                if (i == 11)
                {
                    i = 20;
                }
                Console.WriteLine(i);
            }

            // Prindi arvud 100 kuni 20

            for (int i = 100; i >= 20; i--)
                {
                Console.WriteLine(i);
                }

            // Prindi arcud 1 kuni 100 ( 1,3,5,7...99)
            // i = i + 2 => i += 2
            // i = i - 4 => i -= 4
            // i = i * 2 => i *= 2
            // i = i / 2 => i /= 2

            for (int i = 1; i < 100; i = i + 2)
            {
                Console.WriteLine(i);
            }
            Console.ReadLine();
        }
    }
}
