﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContinueAndBreak
{
    class Program
    {
        static void Main(string[] args)
        {
            while(true)
            {
                Console.WriteLine("HEAD\nAEGA");

                Console.WriteLine("Do you wish to proceed: y/n");
                if (Console.ReadLine().ToLower() != "y")
                {
                    // break hüppab tsüklist välja
                    break;
                }
                else
                {
                    continue;
                }
            }
            Console.ReadLine();

            // for tsükkel mis prindib 1 kuni 10ni numbrid ja jätab vahele nr 5 kasutades continue

            for ( int a = 1; a < 11 ; a++ )
            {

                // kui a on võrdne 5, siis see continue jätab i==5 rea vahele ja jätab WriteLine vahele, kus väärtus on 5
                // kui tahan elimineerida kaks arvu loetelust siis kasutan || (või) arvudele antud värtuste vahel
                // ei saa kasutada && sest a ei saa olla samal ajal 5 ja 7
                // f(i >= 10 && i <=20) i on suurem võrdne kümnest ja(&&) väiksem võrdne 20nest

                if (a == 5 || a == 7)
                {
                    continue;
                }
                    Console.WriteLine(a);
                
            }
            Console.ReadLine();
        }
    }
}
