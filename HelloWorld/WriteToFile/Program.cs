﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace WriteToFile
{
    class Program
    {
        static void Main(string[] args)
        {
            // Otsib faili MinuFail.txt kaustast, kus asub minu exe fail
            // FileMode.Append kirjutab vanale failile juurde
            // FileMode.Create loob alati uue faili ( vana sisu kirjutatakse üle)
            FileStream fileStream = new FileStream("MinuFail.txt", FileMode.Create, FileAccess.Write);
            StreamWriter streamWriter = new StreamWriter(fileStream);

            streamWriter.WriteLine("Kuidas läheb?");
            streamWriter.WriteLine("Läheb hästi!");
            streamWriter.WriteLine("Läheb hästi x 3!");

            streamWriter.Close();
            fileStream.Close();

            string[] lines = new string[] {
                " Elas metsas Mutionu",
                "keset kuuski noori vanu",
                "kadak põõsa juure all",
                "eluruum tal sügaval" };


            // Lühem versioon, see teeb kõik eelnevad asjad, mis üleval kirjas on, teeb ise ka close.
            //File.WriteAllLines("MinuFail.txt", lines);
            File.AppendAllLines("MinuFail.txt", lines);
            File.AppendAllText("MinuFail.txt", " Elas metsas Mutionu\r\n keset kuuski noori vanu\r\n kadak põõsa juure all\r\n eluruum tal sügaval");

            //ContextStaticAttribute void SavePin(string pin)
            //{
            //    string[] lines = new string[] { pin };
            //    // "Peeter ja Jaan ; Juhan, Malle".Split(new string[] { " ja ", ", ", ", "}, StringSplitOptions.None);
            //    // Salvestatakse pin faili
            //    // File.WriteAllLines("pin.txt", pin);
            //    // File.WriteAllLines("pin.txt", new string[] {pin});
            //}
        }
    }
}
