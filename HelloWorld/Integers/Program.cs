﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Integers
{
    class Program
    {
        static void Main(string[] args)
        {
            int number;

            number = 3;

            Console.WriteLine(number);

            // Kaks arvu on 4 ja -7
            int a = 4;
            int b = -7;

            //kaks arvu on 4 ja -7
            Console.WriteLine("Kaks arvu {0} ja {1}", a, b);
            Console.WriteLine("Kaks arvu " + a + " ja " + b);

            // string + string = string
            // string + int = string
            // stringi ja täisarvu liitmisel teisendatakse täisarv stringiks ja siis liidetakse
            //
            // int + int = int
            // matemaatiline arvude liitmine

            // Arvude 4 ja -7 summa on -3
            Console.WriteLine("Kaks arvu {0} ja {1} summa on {2}", a, b, a + b); 
            Console.WriteLine("Kaks arvu " + a + " ja " + b + " summa on " + (a + b));

            // Arvude 4 ja -7 vahe on 11
            Console.WriteLine("Arvude {0} ja {1} vahe on {2}", a, b, (a - b));
            Console.WriteLine("Arvude " + a + " ja " + b + " vahe on " + (a - b));

            // Arvude 4 ja -7 korrutis on -28
            Console.WriteLine("Arvude {0} ja {1} korrutis on {2}", a, b, (a * b));
            Console.WriteLine("Arvude " + a + " ja " + b + " korrutis on " + (a * b));

            // NB Lisasime (double) ja (float), et saada reaalsem tulemus (komakohad)
            // Arvude 4 ja -7 jagatis on -0,57 
            Console.WriteLine("Arvude {0} ja {1} jagatis on {2}", a, b, ((double)a / b));

            // Arvude -7 ja 4 jagatis on -1.75
            Console.WriteLine("Arvude " + a + " ja " + b + " jagatis on " + (b / (float)a));

            // Kahe täisarvu jagamisel on tulemus täisarv, kus kõik peale koma süüakse ära
            // NB Ei toimu ümardamist

            //2147483647 + 1
            int c = 2147483647;
            int d = 2147483646;

            Console.WriteLine( c + d);

            Console.ReadLine();
        }
    }
}
