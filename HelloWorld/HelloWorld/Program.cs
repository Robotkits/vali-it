﻿//annab ligipääsu kõigile klassidele, mis asuvad System nimeruumis
using System;

// Kõik klasside nimed, mis asuvad Helloworld nimeruumis sees
// kehtivad selle nimeruumi piires

namespace HelloWorld// Projektinimi
{
    //Klass nimega Programm(ühes nimeruumis saab olla üks programmi klass)
    class Program 
    {

        // Staatiline meetod
        // void -> meetod ei tagasta midagi
        // Main -> eriline meetod, millest programm alustab tööd
        // string [] args -> meetodi parameetrid ( argumendid)
        static void Main(string[] args) //Methods
        {

            // Kutsutakse välja Console klassi meetod nimega WriteLine()
            // "Tere Maailm" -> Meetodi WriteLine() parameeter ehk tekst, mida välja printida
            Console.WriteLine("Hello World!"); //Commands
            Console.WriteLine("Tere Maailm!");
            // ReadLine() -> ootab kasutajalt vastust ehk teksti, mis lõppeb enteriga
            Console.ReadLine();

            // Kutsub välja Printer nimeruumist klassi Writer meetodi Write()
            Printer.Writer.Write();
            // Kutsub välja File nimeruumist klassi Writer meetodi Write()
            File.Writer.Write();
            // Kutsub välja Database nimeruumist klassi Writer meetodi Write()
            Database.Writer.Write();
        }

        static void Convert()
        {
        }
    }
}
namespace Hello
{
    public class Program
    {
        public static void Convert()
        {
        }
    }
}

namespace Printer
{
    public class Writer
    {
        public static void Write()
        {
        }
    }
}

namespace Database
{
    public class Writer
    {
        public static void Write()
        {
        }
    }
}

namespace File
{
    public class Writer
    {
        public static void Write()
        {
        }
    }
}
