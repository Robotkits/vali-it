﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsList
{
    class Program
    {
        static void Main(string[] args)
        {
            // Masiiv viiest arvust 1, 3, 0, 5, -4
            // Kustuta masiivist esimene number 

            int[] numbers = new int[] { 1, 3, 0, 5, -4 };
            numbers[0] = 0;
            numbers[1] = 0;
            

            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            }

            // Masiiv viiest sõnast "kala", "auto", "","maja", "telefon"
            // Kustuta masiivist esimene sõna

            string[] words = new string[] { "kala", "auto", "", "maja", "telefon" };

            // null on tühihulk, tühjus ehks siis mälu pole eraldatud
            words[0] = null;
            words[0] = words[1];
            words[1] = "vesi";
            for (int i = 0; i < words.Length; i++)
            {
                if (words[i] == null)
                {
                    Console.WriteLine("TÜHI");
                }
                else
                {
                    Console.WriteLine(words[i]);
                }
            }
            //int c;
            string word;

            Console.WriteLine();

            List<int> numbersList = new List<int>() { 1, 3, 22, -2 };

            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            numbersList.Add(5);
            for(int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            numbersList.AddRange(numbers);
            for(int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            numbersList.Remove(1);
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            // eemaldame kohalt 2
            numbersList.RemoveAt(2);
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            // Eemaldame kohast 1 järgnevad 4
            numbersList.RemoveRange(1, 4);
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            // Lisame 1 kohale numbri 1
            numbersList.Insert(1, 1);
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            // Lisame olemas olevale listile teise listi/masiivi
            numbersList.InsertRange(2, numbers);
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            //numbersList.Clear puhastab kõik

            if(numbersList.Contains(1))
            {
                Console.WriteLine("Nimekirjas on olemas number 1");
            }

            // Otsin esimese numberi 33 indeksi
            Console.WriteLine("Numbri 33 indeks on {0}",numbersList.IndexOf(33));

            numbersList.Reverse();
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            numbersList.Sort();
            for (int i = 0; i < numbersList.Count; i++)
            {
                Console.WriteLine(numbersList[i]);
            }
            Console.WriteLine();

            Console.WriteLine(numbersList.ToString());

            Console.WriteLine();


            // Stringidest list
            List<string> wordList = new List<string>();

            // Reaaalarvudest listi
            List<double> realNumbers = new List<double>();

            ArrayList arrayList = new ArrayList() { 3, "maja", 'c', true, 3.45 };
            arrayList.Add(2);
            arrayList.Add("tere");


            int e = -8;
            string f = "kalamees";
            bool isGood = false;

            arrayList.Insert(1, e);
            arrayList.Insert(1, f);
            arrayList.Insert(1, isGood);

            Console.WriteLine();
            for (int i = 0; i < arrayList.Count; i++)
            {
                Console.WriteLine(arrayList[i]);
            }
            Console.WriteLine();

            // arrayList.Remove
            // arrayList.Insert

            arrayList.Remove(4);
            arrayList.Insert(4, "laia");
            if (arrayList[4] is string)
            {
                word = (string)arrayList[4];
                Console.WriteLine(word.ToUpper());
            }

            else if (arrayList[4] is int g)
            {
                g++;
                Console.WriteLine(g);
            }


            //word = (string)arrayList[4];
            //Console.WriteLine(word.ToUpper());

            Console.ReadLine();
        }
    }
}
