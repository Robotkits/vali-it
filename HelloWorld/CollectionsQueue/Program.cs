﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            // Peeter Mari Kaie Paul
            // anna esimene/Peeter
            // järgi jääb Mari Kaie Paul
            // lisandub Jüri
            // Mari Kaie Paul Jüri
            // anna esimene/Mari
            // Kaie Paul Jüri

            Queue<string> queue = new Queue<string>();
            queue.Enqueue("Peeter");
            queue.Enqueue("Mari");
            queue.Enqueue("Kaie");
            queue.Enqueue("Paul");

            // foreach Tsükkel mingi masiivi või muu kollektsiooni läbimiseks
            // Elemente tsükli sees muuta ei saa
            // iga queues oleva stringi kohta  tee muutuja item
            // Ehk iga tsükli korduse sees on string item võrdne järgneva elemendiga

            foreach (string item in queue)
            {
                Console.WriteLine(item);
            }

            string currentClient = queue.Dequeue();

            Console.WriteLine("Teenindatakse klienti {0}", currentClient);

            foreach (string item in queue)
            {
                Console.WriteLine($"Järjekorras on {item}");
            }

            queue.Enqueue("Jüri");

            Console.WriteLine();

            foreach (string item in queue)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            string nextClient = queue.Peek();

            if(nextClient == "Mari")
            {
                string name = queue.Dequeue();
                queue.Enqueue(name);

                Console.WriteLine(nextClient);
            }

            foreach (string item in queue)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();
            Console.ReadLine();
        }
    }
}
