﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringFunctions
{
    class Program
    {
        static void Main(string[] args)
        {
            string sentence = "Elas metas Mutionu!";

            Console.WriteLine("Lauses on {0} sümbolit", sentence.Length);

            // Leia esimese tühiku asukoht (index)

            int spaceIndex = sentence.IndexOf(" ");
            Console.WriteLine("Esimese tühiku indeks on {0}.", spaceIndex);

            // Leia esimesed 3 sümbolit antud lausest

            string subString = sentence.Substring(0, 3);
            Console.WriteLine("Lause esimesed kolm sümbolit on {0}.", subString);

            // Prindib lause iga sümboli erinevale reale

            for (int i = 0; i < sentence.Length; i++)
            {
                Console.WriteLine(sentence[i] + " ");
            }

            string sentenceWithSpaces = "";
            for (int i = 0; i < sentence.Length; i++)
            {
                sentenceWithSpaces += sentence[i];
                if(i == sentence.Length -1)
                {
                    sentenceWithSpaces += " ";
                }

            }

            sentenceWithSpaces += "   ";
            Console.WriteLine("\'{0}\'", sentenceWithSpaces);
            Console.WriteLine("\"{0}\"", sentenceWithSpaces.Trim());

            // proovimiseks
            string[] words = new string[] { "Põdral" + "maja" + "metsa" + "sees" };
            string joinedString = string.Join("!", words);
            Console.WriteLine(joinedString);

            string[] splitWords = joinedString.Split(' ');

            for (int i = 0; i < splitWords.Length; i++)
            {
                Console.WriteLine(splitWords[i]);
            }

            // Küsi kasutajalt nimekiri arvudes, nii, et ta eraldab need tühikuga
            // Liida need kõik arvu koku ja kirjuta vastus
            Console.WriteLine(" Kirjutage jada arvudest, kui olete lõpetanud,\n vajutage ENTER!");
            string answer = Console.ReadLine();
            string[] elements= answer.Split(' ');

            int sum = 0;

            for (int i=0; i < elements.Length; i++)
            {
                Console.WriteLine(elements[i]);
                sum += Convert.ToInt32(elements[i]);
            }

            Console.WriteLine("Summa on {0}", sum);
            // näide $'ga: Console.WriteLine($"Summa on {sum}");

            // Kuidas luua täisaru massiiv, ja tõsta sinna kõik numbrid, mis olid sõnade masiivis
            // kuidas teha käsitsi
            //numbers[0] = Convert.ToInt32(elements[0])
            //numbers[1] = Convert.ToInt32(elements[1])
            //numbers[2] = Convert.ToInt32(elements[2])
            // jne.

            int[] numbers = new int[elements.Length];

            for (int i = 0; i < elements.Length; i++)
            {
                numbers[i] = Convert.ToInt32(elements[i]);
            }

            // Prindi ekraanile järgnev rida
            // answer = "2 4 6 8" => "2+4+6+8=20"

            Console.WriteLine(string.Join(" + ", numbers) + " = " + sum);
            Console.WriteLine(" {0} = {1}", string.Join(" + ", numbers), sum);  // kõige mälu efektiivsem

            Console.Write(string.Join(" + ", numbers));
            Console.Write(" = ");
            Console.Write(sum);


            Console.ReadLine();
        }
    }
}
