﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsStack
{
    class Program
    {
        static void Main(string[] args)
        {
            // "Tõde ja Õigus" "Pipi Pikksukk" "Kroonika"
            // Lisan hunnikusse raamatu "Kalevipoeg"
            // "Tõde ja Õigus" "Pipi Pikksukk" "Kroonika" + "Kalevipoeg"
            // Võtan hunnkust "Kalevipoeg"
            // "Tõde ja Õigus" "Pipi Pikksukk" "Kroonika"
            // Võtan hunnikust "Kroonika"
            // "Tõde ja Õigus" "Pipi Pikksukk" 
            // Võtan hunnikust "Pipi Pikksukk"
            // "Tõde ja Õigus"
            // Stack funktsioon vaatab hunniku peale, kui queue vaatas algusesse

            Stack<string> stack = new Stack<string>();
            stack.Push("Tõde ja Õigus");
            stack.Push("Pipi Pikksukk");
            stack.Push("Kroonika");

            foreach (string book in stack)
            {
                Console.WriteLine(book);
            }
            Console.WriteLine();

            string TopBook = stack.Pop();

            Console.WriteLine("Pealmine raamat oli {0}", TopBook);

            Console.WriteLine();

            foreach (string book in stack)
            {
                Console.WriteLine(book);
            }
            Console.WriteLine();

            Console.WriteLine($"Pealmine raamat on {stack.Peek()}");

            Console.ReadLine();
            
        }
    }
}
