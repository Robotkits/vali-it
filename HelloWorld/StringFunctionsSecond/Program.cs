﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Threading.Tasks;


namespace StringFunctionsSecond
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Sisesta Lause!");
            string sentence = Console.ReadLine().Trim();
            // string sentence = "Elas metsas";
            // string sentence = "Elas metsas Mutionu";

            // Kasutades meetodeid IndexOf, Substring
            // Prindi ekraanile lause esimene sõna

            //int spaceIndex = sentence.IndexOf(" "); // niimoodi saab teada kus esimene tühik on

            //string subString = sentence.Substring(0, spaceIndex); // subString on sõnad lauses.

            ////Console.WriteLine(spaceIndex);

            ////Console.WriteLine(sentence);
            //// Console.WriteLine(spaceIndex);
            //Console.WriteLine(subString);
            //Console.WriteLine(sentence.Substring(0, spaceIndex)); // variant, kuidas kõik kirjeldada kohe ühe lausega
            //Console.ReadLine();

            //// Prindi lause teine sõna( vaja leida kõige pealt teine tühik
            //int firstSpaceIndex = sentence.IndexOf(" ", spaceIndex + 1);
            //int secondSpaceIndex = sentence.IndexOf(" ", spaceIndex + 2);


            //Console.WriteLine(secondSpaceIndex);
            //Console.WriteLine("'" + sentence.Substring(spaceIndex + 1, secondSpaceIndex - spaceIndex) + "'"); // üks tühik on rohkem kui vaja
            //Console.WriteLine("'" + sentence.Substring(spaceIndex + 1, secondSpaceIndex - spaceIndex - 1) + "'"); // korrektne vorm

            //Console.ReadLine();

            if (sentence.Length == 0)
            {
                Console.WriteLine("Lauses pole sõnu");
            }
            else
            {
                int spaceIndex = sentence.IndexOf(" ");
                if (spaceIndex == -1)
                {
                    Console.WriteLine("Esimene sõna on {0}", sentence);
                    Console.WriteLine("Teine sõna on puudu");
                }
                else
                {
                    Console.WriteLine("Esimene sõna on: {0}", sentence.Substring(0, spaceIndex));
                    int secondSpaceIndex = sentence.IndexOf(" ", spaceIndex + 1);
                    if (secondSpaceIndex == -1)
                    {
                        //Substring 1 parameetriga on parameetrist kuni lõpuni
                        Console.WriteLine("Teine Sõna on: {1}", sentence.Substring(spaceIndex + 1));
                    }
                    else
                    {
                        Console.WriteLine("Teine sõna on {0}", sentence.Substring(spaceIndex + 1, secondSpaceIndex - spaceIndex - 1));
                    }

                }
            }
            Console.WriteLine();


            // Kui string.Split ei leia seda sümbolit, millaga ma tahan tükeldada stringi,
            // siis ta tagastab masiivi ühe endiga ja see üks element ongi esialgne terve string

            string[] words = sentence.Split(' '); 
            // lõime uue masiivi ja jagasime ' ' tühikutega sõnad ära

            if (words.Length == 0)
            {
                Console.WriteLine("Lauses pole sõnu");
            }
            else
            {
                Console.WriteLine("Esimene sõna on {0}", words[0]);
                if (words.Length > 1)
                {
                    Console.WriteLine("Teine sõna on {0}", words[1]);
                }
                else
                {
                    Console.WriteLine("Teine sõna on puudu");
                }
            }

            string word = "raudtee";

            Console.WriteLine(word.Replace('r', 'R'));
            Console.WriteLine(word.Replace("Raud", "asfalt"));
            Console.WriteLine("Mina, Pets, Margus, Priit".Replace(",", ""));

            string someString = "Põder jooksis öösel üle ääre";

            Console.WriteLine(someString
                .Replace("ä", "a")
                .Replace('ö', 'o')
                .Replace('õ', 'o')
                .Replace("ü", "u")
                .Replace("Ü", "u")
                .Replace("Õ", "o")
                .Replace("Ä", "a")
                .Replace("Ö", "o")
                );

            Console.WriteLine("Sisesta oma kaal");

            // 79.45
            //79,45

            double weight = Convert.ToDouble(Console.ReadLine().Replace(",", "."), new CultureInfo("en-Us"));

            // :0.00 kohahoidjale juurde pannes saame öelda, et ümarda 2 kohta peale koma
            // {0:0.00} 2 kohta peale koma
            // {0:0.000} 3 kohta peale koma
            // {0:0} 0 kohta peale koma, ümardab täisosani
            Console.WriteLine("Kaal on {0:#.0}", weight);

            for(int i = 0; i < 1000; i++)
            {
                Console.WriteLine("Dokumendi number AA{o}{1:00}{2:00}{3:00000000}",
                    DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 3, i);
            }

            Console.WriteLine("Please enter your phonenumber");
            double phoneNumber = Convert.ToDouble(Console.ReadLine().Replace("372", "").Replace("+", "").Replace(" ", ""));
            Console.ReadLine();

            Console.ReadLine();
        }
    }
}
