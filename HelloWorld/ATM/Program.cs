﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ATM
{
    class Program
    {
        // see on klassi muutuja, kehtib terve klassi piires, ehk kõikides klassi meetoites
        
        static string pin;
        static double balance;

        static void Main(string[] args)
        {

            pin = LoadPin();
            balance = LoadBalance();
            // Faili kirjutamist ja failist lugemist teha alati võimalikult harva
            // paroolid on alati teksti kujul
            Console.WriteLine("Sisesta kaart");

            if (!CheckPinCode())
            {
                return;
            }

            Console.WriteLine("Vali toiming");
            Console.WriteLine("a) Sularaha sissemakse");
            Console.WriteLine("b) Sularaha välja");
            Console.WriteLine("c) Kontojääk");
            Console.WriteLine("d) Katkesta");
            Console.WriteLine("e) Muuda PIN");

            string answer = Console.ReadLine();
            NewMethod(answer);
            //Faili salvestamine
            //balance = balance - 10;
            //SaveBalance(balance);
        }

        private static void NewMethod(string answer)
        {
            switch (answer)
            {
                case "a":
                    {
                        DepositMoneyIN();
                        break;
                    }

                case "b":
                    {
                        DepositMoneyOut();
                        break;
                    }

                case "c":
                    {
                        CheckBalance();
                        break;
                    }

                case "d":
                    {
                        BreakOperation();
                        break;
                    }

                case "e":
                    {
                        if (CheckPinCode())
                        {
                            Console.WriteLine("Palun sisestage oma uus PIN kood:");
                            pin = Console.ReadLine();
                            SavePin(pin);
                            Console.WriteLine($"Teie uus PIN on {pin}");
                        }
                        else
                        {

                            break;
                        }
                        Console.ReadLine();
                        break;
                    }
            }
        }

        private static void BreakOperation()
        {
            Console.WriteLine("Palun võtke oma kaart!");
            Console.ReadLine();
        }

        private static void CheckBalance()
        {
            Console.WriteLine($"Teie kontol on {balance}");
            balance = balance + Convert.ToInt32(Console.ReadLine());
            SaveBalance(balance);
        }

        private static void DepositMoneyIN()
        {
            Console.WriteLine("Sisesta summa");
            balance = balance + Convert.ToInt32(Console.ReadLine());
            SaveBalance(balance);
            Console.WriteLine($"Teie kontol on nüüd {balance}");
            Console.ReadLine();
        }

        private static void DepositMoneyOut()
        {
            Console.WriteLine("Vali millist kupüüri soovid:");
            Console.WriteLine("a) 5");
            Console.WriteLine("b) 10");
            Console.WriteLine("c) 20");
            Console.WriteLine("d) 50");
            Console.WriteLine("e) 100");
            string depositMoney = Console.ReadLine();
            switch (depositMoney)
            {
                case "a":
                    {
                        balance = balance - 5;
                        SaveBalance(balance);
                        Console.WriteLine($"Teie kontole jäi {balance}");
                        Console.ReadLine();
                        break;
                    }
                case "b":
                    {
                        balance = balance - 10;
                        SaveBalance(balance);
                        Console.WriteLine($"Teie kontole jäi {balance}");
                        Console.ReadLine();
                        break;
                    }
                case "c":
                    {
                        balance = balance - 20;
                        SaveBalance(balance);
                        Console.WriteLine($"Teie kontole jäi {balance}");
                        Console.ReadLine();
                        break;
                    }
                case "d":
                    {
                        balance = balance - 50;
                        SaveBalance(balance);
                        Console.WriteLine($"Teie kontole jäi {balance}");
                        Console.ReadLine();
                        break;
                    }
                case "e":
                    {
                        balance = balance - 100;
                        SaveBalance(balance);
                        Console.WriteLine($"Teie kontole jäi {balance}");
                        Console.ReadLine();
                        break;
                    }
                    
            }
        }

        private static bool CheckPinCode()
        {
            for (int i = 0; i < 3; i++)
            {
                Console.WriteLine("Sisestage PIN kood:");
                string enteredPin = Console.ReadLine();
                if (!IsPinCorrect(enteredPin))
                {
                    Console.WriteLine("Vale PIN!");
                    if (i < 3)
                    {
                        Console.WriteLine("Proovi uuesti.");
                    }
                    else
                    {
                        Console.WriteLine("Kaart on konfiskeeritud!");
                        Console.ReadLine();

                        // return lõpetab selle koha peal meetodi töö
                       return false;
                    }
                }
                else
                {
                    return true;
                    //break;
                }
            }
            return true;
        }

        // loeb failist PIN
        static string LoadPin()
        {
            string pinPath = @"C:\Users\admin\Documents\GitHub\vali-it\HelloWorld\ATM\bin\Debug\pin.txt";
            string pin = File.ReadAllText(pinPath);
            return pin;
        }

        // salvestatakse PIN fail
        static void SavePin(string pin)
        {
            string pinPath = @"C:\Users\Admin\Documents\GitHub\vali-it\HelloWorld\ATM\bin\Debug\pin.txt";
            File.WriteAllText(pinPath, (pin));
            
        }

        // loeb failist
        static double LoadBalance()
        {
            string balancePath = @"C:\Users\admin\Documents\GitHub\vali-it\HelloWorld\ATM\bin\Debug\balance.txt";
            double balance = Convert.ToDouble(File.ReadAllText(balancePath));
            return balance;
        }

        // salvestatakse balance fail
        static void SaveBalance(double balance)
        {
            string balancePath = @"C:\Users\admin\Documents\GitHub\vali-it\HelloWorld\ATM\bin\Debug\balance.txt";
            File.WriteAllText(balancePath, Convert.ToString(balance));
        }

        // Kontrollib pin'i failist kus on kasutaja pin
        static bool IsPinCorrect(string enteredPin)
        {
            string pinPath = @"C:\Users\admin\Documents\GitHub\vali-it\HelloWorld\ATM\bin\Debug\pin.txt";
            // ühe reaga
            return enteredPin == pin;
            // //Mitmereaga
            //if (enteredPin == pin)
            //{
            //    return true;
            //}
            // return false;
        }
    }
}
