﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            short a = 1000;
            int b;

            // Implicit conversion
            // toimub iseenesest teisendus
            b = a;

            Console.WriteLine(a);
            Console.WriteLine(b);

            // Explicit conversion conversion
            // pead ise teisendama (cast)
            short c = (short)b;

            // Convert -> teisendad eri tüüpide vahel (tekst ja number) vahel
            // String -> int

            // Cast -> teisendus ( number number)
            // short -> int
            // int -> long

            Console.WriteLine(c);

            b = 35000;
            c = (short)b;
            Console.WriteLine(c);

            // int + short = int
            // int + long = long
            // int * long = long
            // short / byte = int
            // byte / short = int

            // Matemaatilistel tehetel kahe eri mumbri tüübi vahel
            // on tulemus alat suurem tüüp
            // välja arvatud tüübid, mis on väiksemad kui int
            // nende tehete tulemus on alati int

            byte d = 24;
            short e = 1000;

            Console.WriteLine(e / d);
            Console.WriteLine(d / e);

            // GetType() annab infot
            Console.WriteLine(e.GetType());
            Console.WriteLine(e / (d * b));

            uint f = 2000000000;
            uint g = 2000000000;

            ulong h = f * g;

            ushort i = 22;
            ushort j = 3;
            Console.WriteLine((i * j) + " " + (i * j).GetType());
            Console.ReadLine();
        }
    }
}
