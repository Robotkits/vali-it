﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structs
{
    class Program
    {
        static void Main(string[] args)
        {
            Table firstTable = new Table() { Height = 100, Width = 30, NumberOfLegs = 4 };

            Table secondTable = firstTable;

            Console.WriteLine(secondTable.Height);

            secondTable.Height = 40;

            Console.WriteLine(firstTable.Height);
            IncreaseHeight(firstTable, 15);
            Console.WriteLine(firstTable.Height);
            Console.WriteLine(secondTable.Height);


            int a = 3;
            int b = a;
            b = 5;

            Console.WriteLine(a);
            Console.WriteLine();

            List<Table> tables = new List<Table>();
            tables.Add(firstTable);
            tables.Add(secondTable);

            Table thirdTable = tables[0];
            thirdTable.Height = 100;
            tables.Add(thirdTable);

            Console.WriteLine(thirdTable.Height);
            Console.WriteLine(firstTable.Height);
            Console.WriteLine(secondTable.Height);

            tables[1].Height = 70;
            Console.WriteLine();

            foreach (var table in tables)
            {
                Console.WriteLine(table.Height);
            }

            int c = 4;
            Increase(c, 2);
            Console.WriteLine(c);
            Console.WriteLine();

            Chair firstChair = new Chair() { Height = 200, Width = 50, NumberOfLegs = 8 };
            Chair secondChair = firstChair;

            Console.WriteLine(secondChair.Width);

            secondChair.Width = 70;

            Console.WriteLine(secondChair.Width);
            Console.WriteLine(firstChair.Width);

            Console.WriteLine();

            Console.WriteLine(firstChair.Width);
            IncreaseWidth(firstChair, 20);

            Console.WriteLine(secondChair.Width);
            Console.WriteLine(firstChair.Width);

            //// Muutuja ei näita enam arvuti mälus kuhugi
            //// tal ei ole enam väärtust
            //firstTable = null;
            //Console.WriteLine(firstTable.Width);
            //// Lõime teisest lauast uue laua
            //secondTable = new Table();
            //// structile ei saa väärtust null anda.
            //firstChair = null;
            Chair thirdChair;

            Console.ReadLine();
        }

        static void IncreaseHeight(Table table, int HowMuch)
        {
            table.Height += HowMuch;
        }

        static void Increase(int a, int howMuch)
        {
            a += howMuch;
        }

        static void IncreaseWidth(Chair chair, int howMuch)
        {
            chair.Width += howMuch;
        }
    }
}
