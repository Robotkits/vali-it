﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Structs
{
    // Struktuur on väga sarnane klassile
    // Põhi erinevus on see, et struct on value type(lihttüüp)
    struct Chair
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public int NumberOfLegs { get; set; }

        public void FallOver()
        {
            Console.WriteLine("Kukkusin ümber");
        }
    }
}
