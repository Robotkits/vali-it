﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace ConsolArguments
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1. Sõna
            // 2. Mitu korda tahad sõna printida, 10x
            // Kui kasutaja ei ole 2 parameetrit sisestanud
            // Pirnditakse kasutajale
            // Console.WriteLine("Kasutus: {0} [sõna] [mitu korda]", Assembly.GetExecutingAssembly().GetName().Name);
            // Console.ReadLine();

            //if(args.Length != 2) // kui ei kirjuta kahte argumenti; kirjutad if funktsiooni käsu
            //    // kui kirjutad kaks argumenti, liigub else rubriiki
            //{
            //    Console.WriteLine("Kasutus: {0} [sõna] [mitu korda]", Assembly.GetExecutingAssembly().GetName().Name);
            //}
            //else
            //{
            //    for (int i = 0; i < Convert.ToInt32(args[1]); i++)
            //    {
            //        Console.WriteLine(args[0]);
            //    }
            //}

            Console.ReadLine();

            if (args.Length != 2) 
            {
                Console.WriteLine("Kasutus: {0} [sõna] [mitu korda]", Assembly.GetExecutingAssembly().GetName().Name);
            }
            else
            {
                int count;
                bool canParse = int.TryParse(args[1], out count);

                if(canParse)
                {
                    for (int i = 0; i < count; i++)
                    {
                        Console.WriteLine(args[0]);
                    }
                }
                else
                {
                    Console.WriteLine("Kasutus: {0} [sõna] [mitu korda]", Assembly.GetExecutingAssembly().GetName().Name);
                }
                for (int i = 0; i < Convert.ToInt32(args[1]); i++)
                {
                    Console.WriteLine(args[0]);
                }
            }

            Console.ReadLine();
        }
    }
}
