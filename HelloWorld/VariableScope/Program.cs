﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VariableScope
{
    class Program
    {

        // Kõik muutujad, mis ma defineerin, kehtivad/elavad {} piires või sees
        //static void Main(string[] args)
        //{
        //    int a = 4;
        //    if (a ==4)
        //    {
        //        int b = 3;
        //        Console.WriteLine(b);
        //    }
        //    if(a < 5)
        //    {
        //        int b = 5;
        //        Console.WriteLine(b);
        //    }
        //    //Console.WriteLine(b);

        //    Console.ReadLine();

        //}

        static void Main(string[] args)
        {
            int a = 4;

            Increment(a);

            Console.WriteLine("Value of a is {0}", a);

            string sentence = "Mis sa teed";
            AddQuestionMarks(sentence);

            Console.WriteLine("VAlue of sentence is {0}", sentence);

            int[] numbers = new int[] { 2, 4, 23, -11, 12 };

            PrintNumbers(numbers);
            Increment(numbers);
            Console.WriteLine();

            //PrintNumbers(integers);

            int b = 3;
            int c = a;
            c = 7;
            Console.WriteLine(c);


            int[] secondNumbers = numbers;
            secondNumbers[0] = 7;
            Console.WriteLine(numbers[0]);

            Console.ReadLine();

            string firstWord = "raud";
            string secondWord = firstWord;

            firstWord = firstWord + "tee";
            Console.WriteLine(firstWord);

            Console.ReadLine();

            List<int> number = new List<int>() { 2, -4, 0 };
            List<int> secondNumber = number;
            number.RemoveAt(2);
            Console.WriteLine();

            for (int i = 0; i < secondNumber.Count; i++)
            {
                Console.WriteLine(secondNumber);
            }

            Console.ReadLine();
        }

        // Kui meetondile anda parameetriks kaasa lihttüüpi('Value Type') muutuja väärtus,
        // siis tegelikult tehakse uus muutuja ehk koopia sellest muutujast ja pannakse sinna sama väärtus
        // Kui meetodile anda parameetriks kaasa Reference Type muutuja,
        // siis antakse kaasa tegelikult sama muutuja ( uusu muutuja, mis viitab samale mäluaadressile)

        // Value Type - väärtusmuutuja: int, char, string, double, float, decimal, bool, short, long
        // Reference Type - viitmuutuja: masiiivid, listid, dictonary, kõik klassid

        static void Increment(int number)
        {
            number++;

            Console.WriteLine("Newnumber is {0}", number);

            Console.ReadLine();
        }

        static void AddQuestionMarks(string sentence)
        {
            sentence = sentence + "?";

            Console.WriteLine("New sentence is {0}", sentence);

            Console.ReadLine();
        }


        // 
        static void Increment(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = numbers[i] + 1;  // lühem variant on i++ 'ga
                //numbersi++;

                Console.WriteLine(numbers[i]);
            }

            Console.WriteLine();

            Console.ReadLine();
        }
        static void PrintNumbers(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {

            }
        }
    }
}
