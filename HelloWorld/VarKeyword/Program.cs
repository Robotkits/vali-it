﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace VarKeyword
{
    class Program
    {
        enum Gender { Male, Female}
        static void Main(string[] args)
        {
            var a = 3; // int
            var b = 3.0; // double
            var c = 3000000000000000000;
            var d = "kala"; // string ""
            var e = '3'; // char ''
            var f = 3.0f; // float
            var g = 3.0d; // double
            var h = 3.0m; // decimal

            var numbers = new[] { 1, 2, 3, 4};
            var doubles = new int[3];

            Console.WriteLine(doubles);
            Console.ReadLine();
            //var määrab ise tüübi
            // Täisarvu puhul pannakse väärtuseks int
            // või kui ei mahu pannakse long
            Console.WriteLine(a.GetType());
            Console.WriteLine(b.GetType());
            Console.WriteLine(c.GetType());
            Console.WriteLine(d.GetType());
            Console.WriteLine(e.GetType());
            Console.WriteLine(f.GetType());
            Console.WriteLine(g.GetType());
            Console.WriteLine(h.GetType());
            Console.ReadLine();

            FileStream fileStream = new FileStream("minufail.txt", FileMode.Append, FileAccess.Write);

            //Gender gender = (int)Gender.Female;
            //var gender = Gender.Female;

            Console.ReadLine();
        }
    }
}
