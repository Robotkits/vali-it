﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inheritance;

namespace BoxingUnboxing
{
    // References näitab, mis dll või exesi mu projekt kasutab
    class Program
    {
        static void Main(string[] args)
        {
            // boxing/unboxing on ressurssi nõudvam protsess
            int a = 3;

            // object tüüpi muutujasse saab panna ükskõik mis .NET defineeritud muutujat/väärtust
            // boxing
            object b = a;

            // unboxing
            int c = (int)b;

            Console.WriteLine(b);
            Print(123);
            Print(false);
            Print("Tere");
            Print(12.75);

            Cat cat = new Cat
            {
                Name = "Miisu"
            };

            object e = cat; // boxing
            Cat sameCat = (Cat)e;   // unboxing

            // tehakse samamoodi boxing
            object[] objects = new object[] { 2, "tere", false, cat };

            //unboxing
            Console.WriteLine((string)objects[1]);
            Console.WriteLine((bool)objects[2]);
            Console.WriteLine(((Cat)objects[3]).Name);

            Animal firstAnimal = new Cat();
            Animal secondAnimal = new Lion();

            List<Animal> animals = new List<Animal>
            {
                new Cat() { LivesRemaining = 8, Name = "KAti" },
                new Dog() { FavoriteToy = "KARU", Name = "Peeter" },
                firstAnimal,
                secondAnimal
            };

            foreach (var animal in animals)
            {
                animal.Eat();
            }

            Dog thirdAnimal = (Dog)animals[1];
            Console.WriteLine(thirdAnimal.FavoriteToy);

            // Saabteisendada tüüpe kõikide tõõpide vahel, mis on pärilusega seotud
            // Kassi ei saa teisendada koeraks, sest nad ei pärine teineteisest
            // Dog dog = (Dog)cat;
            Wolf wolf = new Wolf();
            WildAnimal wildAnimal = wolf;
            Wolf secondWolf = (Wolf)wildAnimal;
            secondWolf.Howl();

            Carnivore carnivore = (Carnivore)wildAnimal;
            Wolf thirdWolf = (Wolf)carnivore;

            // Kõik see võimaldab meil asju erimoodi grupeerida

            Console.ReadLine();
        }
        static void Print(object obj)
        {
            if(obj is string)
            {
                Console.WriteLine("See string on {0}", (string)obj);
            }
            else if (obj is bool)
            {
                Console.WriteLine("See boolean on {0}", (bool)obj);
            }
            else if(obj is int)
            {
                Console.WriteLine("See Int on {0}", (int)obj);
            }
            else
            {
                Console.WriteLine("See double on {0}", (double)obj);
            }
            Console.ReadLine();
        }
    }
}
