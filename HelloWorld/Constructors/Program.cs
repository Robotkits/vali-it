﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inheritance;


// Konstruktor on eriline meetod, mis käivitatakse objekti loomisel
// Kasutatakse näiteks mingite muutujate algväärtustamiseks

namespace Constructors
{
    class Program
    {
        static void Main(string[] args)
        {
            // Dog() tähendabki parameetrita konstruktori välja kutsumist
            Dog tax = new Dog();
            Dog buldog = new Dog();
            Dog boxer = new Dog("Naki", 2);

            Console.WriteLine("Taxi nimi on {0}, vanus on {1}", tax.Name, tax.Age);
            Console.WriteLine("Boxer nimi on {0}, vanus on {1}", boxer.Name, boxer.Age);

            Console.ReadLine();


        }
    }
}
