﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inheritance;

namespace LivingPlace
{
    interface ILivingPlace
    {
        int MaxAnimalCount { get; }

        void AddAnimal(Animal animal);
        void PrintAnimals();
        int GetAnimalCount(string animalType);
        void RemoveAnimal(string animalType);
    }
}
