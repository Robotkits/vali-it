﻿using Inheritance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LivingPlace
{
    public class Farm : ILivingPlace
    {

        private List<Animal> animals = new List<Animal>();
        private Dictionary<string, int> animalCounts = new Dictionary<string, int>();

        public int MaxAnimalCount { get; private set; }

        public Farm(int count = 25)
        {
            MaxAnimalCount = count;
        }
        // 1. Lisada loomi, ainult FarmAnimal klassis olevad loomad
        public void AddAnimal(Animal animal)
        {
            // is kontrollib, kas antud võrreldav objekt animal pärineb või on tüübist FarmAnimal
            if(!(animal is FarmAnimal))
            {
                Console.WriteLine("Farmis saavad elada ainult farmi loomad");
                return;
            }
            if (animals.Count < MaxAnimalCount)
            {
                // Uue looma lisamine
                // Kontrollin, et loomi Kokku ei oleks rohkem kui Max
                // animalCounts.Count näitab alati, mitu erinevat looma mul on
                if (!animalCounts.ContainsKey(animal.GetType().Name) && animalCounts.Count < 4)
                {
                    animals.Add(animal);
                    animalCounts.Add(animal.GetType().Name, 1);
                }

                // animalCounts(animal.GetType().Name
                //if (animals.Count < MaxAnimalCount && animalCounts(animal.GetType().Name < 5)
                else if (animalCounts.ContainsKey(animal.GetType().Name) && animalCounts[animal.GetType().Name] < 5)
                {
                    animals.Add(animal);
                    animalCounts[animal.GetType().Name]++;
                }
                else
                {
                    Console.WriteLine("Loom ei mahu farmi");
                }
            }
            else
            {
                Console.WriteLine("Farmis on juba maksimum arv loomi");
            }
            Console.WriteLine("{0} lisati Farmi", animal.GetType().Name);
                //// animalCounts.Count näitab alati, mitu erinevat looma mul on
                //if (!animalCounts.ContainsKey(animal.GetType().Name) && animalCounts.Count < 4)
                //{
                //    animalCounts.Add(animal.GetType().Name, 1);
                //}
                //else
                //{
                //    // wordCounts[word] = wordCounts[word] + 1;
                //    // wordCounts[word] += 1;
                //    animalCounts[animal.GetType().Name]++;
                //}
        }
            // 2. Küsida mis loomad on
            // prindib loomatüübi koos arvuga
            // Cow 4
            // Pig 2

        public void PrintAnimals()
        {
            foreach (var animalCount in animalCounts)
            {
                Console.WriteLine("{0} {1}", animalCount.Key, animalCount.Value);
            }
        }

        // 3. Küsida loomade arvu

        public int GetAnimalCount(string animalType)
        {
            if(animalCounts.ContainsKey(animalType))
            {
                return animalCounts[animalType];
            }
            Console.WriteLine("Looma {0} ei Leitud", animalType);
            return 0;
        }

        // 4. Eemaldada loomi
        public void RemoveAnimal(string animalType)
        {
            if(!animalCounts.ContainsKey(animalType))
            {
                Console.WriteLine("Looma {0} ei leitud", animalType);
                return;
            }
            //animals.Remove(FarmAnimal animal)
            //animals.RemoveAt(int index)
            for (int i = 0; i < animals.Count; i++)
            {
                if( animals[i].GetType().Name == animalType)
                {
                    // animals.RemoveAt(animals.IndexOf(animals[i]));
                    // animals.RemoveAt[i];
                    animals.Remove(animals[i]);
                    Console.WriteLine("Loom {0} lisati Farmi", animals.GetType().Name);
                    // Kui on viimane loom ehk dictionarys on kogus 1
                    // siis eemalda dictionaryst
                    if (animalCounts[animalType] == 1)
                    {
                        animalCounts.Remove(animalType);
                    }
                    // muul juhul vähenda kogust 1 võrra
                    else
                    {
                        animalCounts[animalType]--;
                    }
                    break;// kustutame ainult esimese, kui breaki pole, siis kustutaks kõik seda tüüpi
                }
            }
        }
        // 5. Täienda koodi nii, et famis saab olla 4 erinevat looma ja igatühte max = 5tk

        // 6. Täienda remove animal nii, et kui sellist looma ei leitud, siis prindib " Looma ei leitud"



        // 7. Täiendada GetAnimalCount(string animalType), nii et kui sellist looma ei leitud, siis prindib "Looma ei leitud"
        // samas tagastab -1
        // 8. Looge klassid Forest ja Zoo ja mõelge, kuidas teha interface ILivingPlace, kus on kõik kirjas kõik ühine
        // Farm, Zoo ja Forest jaoks
    }
}
