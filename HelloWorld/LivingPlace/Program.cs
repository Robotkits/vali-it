﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inheritance;

namespace LivingPlace
{
    class Program
    {
        static void Main(string[] args)
        {
            ILivingPlace livingPlace = new Forest();
            livingPlace.AddAnimal(new Cow());
            livingPlace.AddAnimal(new Cow());
            livingPlace.AddAnimal(new Sheep());
            livingPlace.AddAnimal(new Sheep());
            livingPlace.AddAnimal(new Chicken());
            livingPlace.AddAnimal(new Chicken());
            livingPlace.AddAnimal(new Pig());
            livingPlace.AddAnimal(new Pig());
            livingPlace.AddAnimal(new Sheep());
            livingPlace.AddAnimal(new Sheep());
            livingPlace.AddAnimal(new Chicken());
            livingPlace.AddAnimal(new Chicken());
            livingPlace.AddAnimal(new Pig());
            livingPlace.AddAnimal(new Pig());
            livingPlace.AddAnimal(new Cow());
            livingPlace.AddAnimal(new Cow());
            livingPlace.AddAnimal(new Sheep());
            livingPlace.AddAnimal(new Sheep());
            livingPlace.AddAnimal(new Chicken());
            livingPlace.AddAnimal(new Chicken());
            livingPlace.AddAnimal(new Cow());
            livingPlace.AddAnimal(new Sheep());
            livingPlace.AddAnimal(new Sheep());
            livingPlace.AddAnimal(new Chicken());
            livingPlace.AddAnimal(new Chicken());
            livingPlace.AddAnimal(new Lion());

            livingPlace.PrintAnimals();

            Console.WriteLine();

            livingPlace.RemoveAnimal("Cow");
            livingPlace.RemoveAnimal("Cow");
            livingPlace.RemoveAnimal("Cow");
            livingPlace.RemoveAnimal("Cow");
            livingPlace.RemoveAnimal("Cow");
            livingPlace.RemoveAnimal("Cow");

            livingPlace.PrintAnimals();

            ValueType count = livingPlace.GetAnimalCount("Chicken");




            Console.ReadLine();
        }
    }
}
