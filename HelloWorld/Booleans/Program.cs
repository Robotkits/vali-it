﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booleans
{
    class Program
    {
        static void Main(string[] args)
        {
            // bool on sama, mis Boolean
            //väärtus kas tõene või vale

            bool isMinor;

            Console.WriteLine("Mis on sinu vanus?");

            int age = Convert.ToInt32(Console.ReadLine());

            if (age < 18)
            {
                isMinor = true;
            }
            else
            {
                isMinor = false;
            }

            Console.WriteLine(isMinor);

            if (isMinor)
            {
                Console.WriteLine("Oled alaealine");
            }
            else
            {
                Console.WriteLine("Oled täisealine");
            }
            
            bool haveEaten = false;

            Console.WriteLine("kas sa täna hommikusööki sõid?");
            Console.WriteLine("Vasta j/e");

            if (Console.ReadLine() == "j")
            {
                haveEaten = true;
            }

            Console.WriteLine("kas sa täna lõunasööki sõid?");
            Console.WriteLine("Vasta j/e");

            if (Console.ReadLine() == "j")
            {
                haveEaten = true;
            }

            Console.WriteLine("kas sa täna õhtusööki sõid?");
            Console.WriteLine("Vasta j/e");

            if (Console.ReadLine() == "j")
            {
                haveEaten = true;
            }

            if (haveEaten)
            {
                Console.WriteLine("Mis sa arvad, kas sa võid seda söömiseks nimetada?");
                Console.WriteLine("vasta j/e");
                if (Console.ReadLine() != "j")
                {
                    haveEaten = false;
                }
            }

            if (haveEaten)
            {
                Console.WriteLine("Sa oled täna söönud");
            }
            else
            { Console.WriteLine("Sa ei ole täna söönud");
            }

            // Console.WriteLine() oskab väärtuse true või false välja printida

            bool a = 3 == 3;
            int number = 2;

            bool b = (number < 6 || number > 8) && number != 4;

            bool c = !true;

            string words = "head aega";
            bool d = !(words == "tere");

            if (!(false || a && b || c))
            {

            }

            Console.ReadLine();

        }
    }
}
