﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadFromFile
{
    class Program
    {
        static void Main(string[] args)
        {
            // lisasime teksti/pathi ette @ ja ei pea kaldkriipsu ette kirjutama teist kaldkriipsu \\ seda on vaja kasutada näite 2 ja 3 puhul
            string filePath = @"C:\Users\admin\Documents\GitHub\vali-it\newTextDocument.txt";

            // näide 1
            FileStream fileStream = new FileStream("C:\\Users\\admin\\Documents\\GitHub\\vali-it\\newTextDocument.txt", FileMode.Open,FileAccess.Read);
            StreamReader streamReader = new StreamReader(fileStream, Encoding.Default);

            // Iga kord kui ma kutsun uuesti stramReader.ReadLine(), loetakse järgmine rida
            // Kui järgmist rida ei ole, siis streamReader.ReadLine() tagastab null

            string line = streamReader.ReadLine();

            // võimalus, seda pole mõtet selleks kasutada, liiga keeruline keskmisele arendajale
            //for (; line != null; )
            //{
            //    Console.WriteLine(line);
            //    line = streamReader.ReadLine();
            //}

            while (line != null)
            {
                Console.WriteLine(line);
                line = streamReader.ReadLine();
                //line = streamReader.ReadLine();
                //Console.WriteLine(line);
            }

            streamReader.Close();
            fileStream.Close();

            // Näide 2
            Console.WriteLine(File.ReadAllText(filePath));

            //Näide 3
            string[] lines = File.ReadAllLines(filePath);

            for (int i = 0; i < lines.Length; i++)
            {
                Console.WriteLine(lines[i]);
            }

            Console.ReadLine();
        }
    }
}
