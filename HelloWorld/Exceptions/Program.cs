﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// Exceptions on programmi töös erinev erijuht
// mille esinemisega ma peaks arvestama ja tegelema
namespace Exceptions
{
    class Program
    {
        static void Main(string[] args)
        {
            // Try plokk eraldab koodi osa, kus me arvame,
            // et  võib juhtuda exception
            try
            {
                //string word = " ";
                string word = null;
                word.Split(' ');
                Console.WriteLine("Sõna oli {0}", word);
            }
            // Püüab kinni erandi
            // tüübist NullReferenceException
            // ehk selline tüüp erandist, mis juhtub, kui tahame kas mingit meetodit
            // või parameetrit küsida millelt, mis on parasjagu null
            catch (NullReferenceException ex)
            {
                Console.WriteLine("Viga word on null: " + ex.Message);
            }

            int a = 3;
            int b = 5;

            Console.WriteLine("arvude summa on {0} ", a + b);
            Console.ReadLine();
            //string word = null;
            //word.Split(' ');

            a = 5;

            //Console.WriteLine("Sisesta arv, millega tahad numbrit 5 jagada:");
            //b = Convert.ToInt32(Console.ReadLine());
            //try
            //{
            //    Console.WriteLine("Arvude 5 ja {0} jagatis on {1}", b, a / b);
            //}
            //catch (DivideByZeroException ex)
            //{
            //    Console.WriteLine("Viga, 0-ga ei saa jagada: " + ex.Message);
            //}




            try
            {
                Console.WriteLine("Sisesta arv, millega tahad numbrit 5 jagada:");
                b = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Arvude 5 ja {0} jagatis on {1}", b, a / b);
                int[] numbers = new int[2];
                numbers[2] = 3;
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Viga, tähega pole võimalik jagada: " + ex.Message);
            }
            catch (DivideByZeroException ex)
            {
                Console.WriteLine("Viga, ei saa jagada: " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Viga, süsteemi error: " + ex.Message);
            }
            
            // 
            finally
            {
                Console.WriteLine("Finaal");
            }
            Console.ReadLine();
        }
    }
}
