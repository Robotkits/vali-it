﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Loend või massiiv. Mingi hulk näiteks numbreid, sõnu vms.
namespace Arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            // Luuakse täisarvude massiiv numbers, millesse mahub 5 täisarvu
            // Loomise hetkel, pean määrama kui suure (mitu numbrit mahub) massiivi teen
            int[] numbers = new int[5];

            // Massiivi indeksid algavad 0 ( nad on [0] vahel)
            // Viimane indeks on alati 1 võrra väiksem kui massiivi maksumum elementide arv
            numbers[0] = 1;
            numbers[1] = 2;
            numbers[2] = -1;
            numbers[3] = -11;
            numbers[4] = 12;

            Console.WriteLine(numbers[2]);
            // 1. Prindi kõik numbrid
            // numbers.Lenght küsib automaaselt massiivi suuruse

            Console.ReadLine();
            for(int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            }
            Console.ReadLine();

            //2.prindi tagurpidi


            for (int i = numbers.Length - 1; i >= 0; i--)
            {
                Console.WriteLine(numbers[i]);
            }
            Console.ReadLine();

            // 3.Prindi kõik positiived numbrid

            for (int i = 0; i < numbers.Length; i++)
            {
                if(numbers[i] > 0)
                {
                    Console.WriteLine(numbers[i]);
                }
            }
            Console.ReadLine();



            // 4. Loo teine massiiv 3le numbrile ja pane sinna esimese masiiivi kolm esimest numbrit.

            int[] newNumbers = new int[3];

            for(int i =0; i < newNumbers.Length; i++)
            {
                newNumbers[i] = numbers[i];
                Console.WriteLine(newNumbers[i]);
            }
            Console.ReadLine();

            // 5. Loo teine massiiv 3le numbrile ja pane sinna esimese masiivi 3 numbrit
            // tagant poolt alates. Prindi teise massiivi numbrid ekraanile.

            int[] oldNumbers = new int[3];

            for (int i = 0; i < oldNumbers.Length; i++)
            {
                oldNumbers[i] = numbers[numbers.Length - 1 - i];
                Console.WriteLine(oldNumbers[i]);
            }

            int a = 4;
            for (int i = 0, j = 4; i < oldNumbers.Length; i++)
            {
                oldNumbers[i] = numbers[j];
                a--;
            }

            for (int i = 0, j = 4; i < oldNumbers.Length; i++, j--)
            {
                oldNumbers[i] = numbers[j];
            }

            Console.WriteLine();

            for (int i = 0; i < oldNumbers.Length; i++)
            {
                Console.WriteLine(oldNumbers[i]);
            }

            Console.ReadLine();
        }
    }
}
