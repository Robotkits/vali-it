﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoWhileLoop
{
    class Program
    {
        static void Main(string[] args)
        {

            bool exitProgram = false;

            // kirjuta do, topelt TAB ja paneb ise loogsulud ja while(true) paika

            // do(while) tükkel erineb tavalisest while tsüklist selle poolest,
            // et esimene tsükli kordus tehakse alati olenemata kas kontrollitav tingimus on 
            // tüene või vale

            // Kasutatakse siis, kui mingi tegevus on tehtud ning tahan otsustada, kas teen seda veel
            do
            {
                Console.WriteLine("TERE");
                Console.WriteLine("Kas soovid jätkata? j/e");
                if(Console.ReadLine() != "j")
                {
                    exitProgram = true;
                }
            } while (!exitProgram);

            //while (!exitProgram)
            //{
            //    Console.WriteLine("TERE");
            //    Console.WriteLine("Kas soovid jätkata? j/e");
            //    if (Console.ReadLine() != "j")
            //    {
            //        exitProgram = true;
            //    }
            //}

            Console.ReadLine();
        }
    }
}
