﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinMaxValue
{
    class Program
    {
        static void Main(string[] args)
        {
            // Leia massiivist suurim number
            int[] numbers = new int[] { 1, 3, -12, 0, 12, 2, -17 };

            //int max = numbers[0];
            //int counter = 0;

            //for ( int i = 1; i < numbers.Length ; i++)
            //{
            //    if (numbers[i] > max)
            //    {
            //        max = numbers[i];
            //        counter = i + 1;
            //    }
            //}

            int max = int.MinValue;
            int counter = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] > max)
                {
                    max = numbers[i];
                    counter = i + 1;
                }
            }

            // Leia suurim paaritu arv
            // if(arv % 2 !== 0)
            Console.WriteLine("Jääk on {0}", 9 % 2);

            Console.WriteLine("Suurim number on {0}, mis on järjekorras {1}.", max, counter);

            // Leia väikseim arv järjekorras

            int min = int.MaxValue;
            counter = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] < min)
                {
                    min = numbers[i];
                    counter = i - 1;
                }
            }

            Console.WriteLine("Väikseim on {0}, mis on järjekorras {1}.", min, counter);

            max = int.MinValue;
            counter = -1;

            for(int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] > max && numbers[i] % 2 == 0)
                {
                    max = numbers[i];
                    counter = i + 1;
                }
            }
            if(max == int.MinValue && counter == -1)
            {
                Console.WriteLine("Paarituid arve pole");
            }
            else
            {
                Console.WriteLine("Suurim paaritu arv on {0}, mis järjekorras on {1}.", max, counter);
            }

            Console.ReadLine();
        }
    }
}
