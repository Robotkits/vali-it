﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car
            {
                Model = "525",
                Make = "BMW",
                Year = 1991
            };

            car.StartEngine();
            car.StartEngine();



            if (car.IsEngineRunning)
            {
            Console.WriteLine("Auto hetkel töötab");
            }

        Console.WriteLine($"Auto kiirus on hetkel {car.Speed}");
        car.Accelerate(200);
        Console.WriteLine($"Auto kiirus on hetkel {car.Speed}");
        car.SlowDown(50);
        Console.WriteLine($"Auto kiirus on hetkel {car.Speed}");

        Person driver = new Person
        {
            FirstName = "Kalle",
            LastName = "Kaalikas",
            Age = 25
        };
        Person owner = new Person()
        {
            FirstName = "Malle",
            LastName = "Maasikas",
            Age = 30
        };
        Person isa = new Person()
        {
            FirstName = "Teet",
            LastName = "Tiit",
            Age = 30
        };
        Person ema = new Person()
        {
            FirstName = "Maiu",
            LastName = "Piimaauto",
            Age = 30
        };
        Person vend = new Person()
        {
            FirstName = "Peep",
            LastName = "Vaid",
            Age = 30
        };
        Person isaIsaOne = new Person()
        {
            FirstName = "VLAD",
            LastName = "Valu",
            Age = 100
        };
        Person isaIsaTwo = new Person()
        {
            FirstName = "VITALI",
            LastName = "Vanker",
            Age = 150
        };
        car.Driver = driver;
        car.Owner = owner;

            Console.WriteLine($"Auto {car.Make} omaniku nimi on {owner.FirstName} {owner.LastName}");
            // Võimalus, kuidas luua inimeste listi, näide:

            var passengers = new List<Person>()
        {
            new Person()
            {
            FirstName = "Mare",
            LastName = "Peterson",
            Age = 45
            },
            new Person()
            {
            FirstName = "Allan",
            LastName = "Peterson",
            Age = 55
            },
            new Person()
            {
                FirstName = "Toomas",
                LastName = "Toomingas",
                Age = 22
            }
        };
        car.Passengers = passengers;

        // prindi välja auto omaniku perekonnanimi
        // Lisage personile ema ja isa ning lisa see sama person autojuhiks
        // autojuhi emale ja isale lisa samuti isa
        // ning küsi auto objekti käest, mis on selle autojuhi mõlemate vanaisade eesnimed
        Console.WriteLine($"Auto omaniku {owner.FirstName} {owner.LastName}, vanaisade eesnimed on {isaIsaOne.FirstName} vanust {isaIsaOne.Age} ja {isaIsaTwo.FirstName} vanust {isaIsaTwo.Age}");

        for (int i = 0; i < car.Passengers.Count; i++)
        {
            Console.WriteLine("{0} reisija vanus on {1}", i + 1, car.Passengers[i].Age);
        }

        foreach (var passenger in car.Passengers)
        {
            Console.WriteLine(passenger.Age);
        }

            Console.WriteLine($"Reisijate vanused on: {car.Passengers[0].Age}, {car.Passengers[1].Age}, {car.Passengers[2].Age}");

            // Prindi välja autojuhi kõikide meesliini esiisade nimed
            car.Driver.Father = new Person()
            {
                FirstName = "Roomet",
                Father = new Person()
                {
                FirstName = "Jaan"
                },
                Mother = owner
            };

            car.Driver.Mother = new Person
            {
                FirstName = "Tiiu",
                Father = new Person
                {
                    FirstName = "Juhan"
                }
            };

            // Autojuhi vanaisa nime muutmine
            car.Driver.Father.Father.FirstName = "Peeter";
            Console.WriteLine(car.Driver.Father.Father.FirstName);
            Console.WriteLine(car.Driver.Mother.Father.FirstName);

            // Prindi välja autojuhi kõikide meesliini esiisade nimed

            car.Driver.Father.Father.Father = new Person
            {
                FirstName = "John",
                Father = new Person
                {
                    FirstName = "Jake"
                }
            };

            Console.WriteLine();

            Person.PrintFatherName(car.Driver);

            // Jake, John, Peeter, Roomet
            //car.Driver.Father.Father.Father.Father

            //string.Join(" ", new string[] { "a", "b" });
            //string sentence = "Juku ja Juhan";
            //sentence.Split(' ');

            //Console.WriteLine(sentence);

        Console.ReadLine();
            
        }
    }
}
