﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassMethods
{
    enum Gender {  Male, Female, Undislosed}

    class Person
    {
        public Gender Gender { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public Person Father { get; set; }
        public Person Mother { get; set; }

        // Rekursiivne meetod
        // Ehk meetod, mis kutsub iseennast uuesti välja

        // Staatiline meetod ( static) on selline meetod, mille välja kutsumiseks ei ole objekti vaja luua
        //
        static public void PrintFatherName(Person person)
        {
            if (person.Father != null)
            {
                Console.WriteLine(person.Father.FirstName);
                PrintFatherName(person.Father);
            }
        }
    }
}
