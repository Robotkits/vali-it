﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassMethods
{
    class Car
    {
        // private tähendab, et muutuja või meetod on ainult ligipääsetav sama klassi sees
        // üleval lühike variant, kuid allpool pikem variant koos täiendustega
        // lühikesele variandile saab alati infot juurde panna kui vaja
        // private set tähendab, et seda set meetodit saab välja kutsuda
        // ainult klassi enda seest

        // lisage property, kus hoitakse auto hetke kiirust ja kust saab küsida
        // lisage meetod kiirendamiseks, kus saab määrata kui suure kiiruseni kiirendan
        // lisage meetod aeglustamiseks.
        // lülita mootor välja
        // tee diagnostika ( kontrollib õli taset, coolanti tase) nt alla 50% 
        private int year;
        // private int weight;

        public string Model { get; set; }
        public string Make { get; set; }
        public int Year
        {
            get
            {
                return year;
            }
            set
            {
                if (Year < 1880)
                {
                    year = 1880;
                }
                else if (value > DateTime.Now.Year)
                {
                    year = DateTime.Now.Year;
                }
                else
                {
                    year = value;
                }
            }
        }

        public bool IsEngineRunning { get; private set; }
        public bool EngineIsNotRunning { get; set; }
        public int Speed { get; private set; }
        public Person Owner { get; set; }
        public Person Driver { get; set; }
        public List<Person> Passengers { get; set; }

        public void StartEngine()
        {
            if (IsEngineRunning)
            {
                IsEngineRunning = false;
                Console.WriteLine("Mootor juba töötab!!!");
            }
            else
            {
                IsEngineRunning = true;
                Console.WriteLine("Mootor käivitus");
            }
        }

        public void Accelerate(int speed)
        {
            if (!IsEngineRunning)
            {
                Console.WriteLine("Auto mootor ei tööta, ei saa hetkel kiirendada");
            }
            else if (Speed > speed)
            {
                Console.WriteLine("Ei saa kiirendada aeglasemale kiirusele");
            }
            else if (speed < 0)
            {
                Console.WriteLine("Auto kiirus ei saa olla negatiivne");
            }
            else if (speed > 400)
            {
                Console.WriteLine("Auto nii kiiresti ei sõida");
            }
            else
            {
                Speed = speed;
                Console.WriteLine($"Auto kiirendas kuni kiiruseni {speed}");
            }
        }

        public void SlowDown (int speed)
        {
            if(!IsEngineRunning)
            {
                Console.WriteLine("Auto mootor ei tööta, ei saa hetkel aeglustada!");
            }
            else if (Speed < speed)
                    {
                        Console.WriteLine("Ei saa aeglustada kiiremale kiirusele");
                    }
            else if (speed< 0)
            {
                Console.WriteLine("Auto mootor ei tööta, ei saa hetkel aeglustada!");
            }
            else if (speed > 400)
            {
                Console.WriteLine("Auto nii kiiresti ei sõida!");
            }
            else
            {
                Speed = speed;
                Console.WriteLine($"Auto aeglustus kuni kiiruseni {speed}");
            }
        }
    }
}
