﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IfElse
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Sisestage palun oma nimi!");
            string name = Console.ReadLine();

            // if(name.ToUpper() == myName.ToUpper() "ott" -> "OTT"
            // if (name == "Ott Allan" || name == "ott allan")

            if (name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
            {
                Console.WriteLine("Tere, kasutaja Ott");

                // Küsi vanust ning kui vanus on alaealine,siis
                // kirjuta, et sisse ei pääse
                // muuljuhul, kirjuta tere tulemast klubisse.

                Console.WriteLine("Mis on teie vanus?");

                int age = Convert.ToInt32(Console.ReadLine());
                if (age >= 18)
                {
                    Console.WriteLine("Tere tulemast klubisse!");
                }
                else
                {
                    Console.WriteLine("Vabandage, te olete liiga noor!");
                }
            }            
            else           
            {
                Console.WriteLine("Ma ei tunne sind");

                Console.WriteLine("Mis on Sinu Perekonnanimi?");
                string LastName = Console.ReadLine();
                if (LastName == "Mahlapuu" || LastName == "mahlapuu")
                {
                    Console.WriteLine("Tere Ott Allan Mahlapuu");
                }
                else
                {
                    Console.WriteLine("Ma ikkagi ei tunne sind!");
                }
                    // Küsib prekonnanime ning kui perekonnanimi
                    // klapib, siis ütle tere Ott Allan Mahlapuu. muuljuhul,
                    // ütleb ma ikkagi ei tunne sind

            }

            Console.ReadLine();
        }
    }
}
