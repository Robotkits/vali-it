﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace Conditions
{
    class Program
    {
        static void Main(string[] args)
        {
            // Seadista programmi keeleks USA inglise keel
            CultureInfo.CurrentCulture = new CultureInfo("en-US");

            Console.WriteLine("Sisesta palun 1 number");
            string answer = Console.ReadLine();

            // asendab tekstis kõik komad punktidega
            answer = answer.Replace(",", ".");

            double a;

            // Kontrolli, kas programmi keel on Eesti eesti keel
            if (CultureInfo.CurrentCulture.Name == "et-EE")
            {
                // kasuta , reaalarvus
                a = Convert.ToDouble(answer);
            }
            else
            {
                // kasuta . reaalarvus
                // lisaparameeter konverteerimiskeelena
                a = Convert.ToDouble(answer, new CultureInfo("en-US"));
            }

            Console.WriteLine(a);

            // Alati kui järgneb loogulg, siis sinna ei panda semikoolonit!!!!
            if (a == 3.24)
            {
                // Kui a on võrdne kolmega, siis käivitab kõik järgneva koodi, mis sulgude vahel on
                // Kui a ei ole võrdne kolmega, siis ta läheb sellest osast üle
                Console.WriteLine("Arv on võrdne kolmega");
            }

            if(a != 4)
            {
                Console.WriteLine("Arv ei ole võrdeline neljaga");
            }
            if(a > 2)
            {
                Console.WriteLine("Arv on suurem kahest");
            }

            if( a > 6)
            {
                Console.WriteLine("Arv on väiksem kuuest");
            }

            // Kui arv on suurem võrdne 5ga
            if (a >= 5)
            {
                Console.WriteLine("Arv on suurem viiest");
            }

            if (a != 6)
            // Alati kui järgneb loogsulg, siis sinna ei panda semikoolonit!!!
            {
                Console.WriteLine("Arv ei ole võrdeline kuuega");
            }

            // Kui arv on väiksem kui 2 või suurem kui 8
            // || -> VÕI
            // && -> JA
            if (a > 8 || a < 2)
            {
                Console.WriteLine("Arv väiksem kui 2 või suurem kui 8");
            }

            // Kui arv on väiksem kui 2 või suurem kui 8
            if (a >2 && a < 8)
            {
                Console.WriteLine("Arv on 2 ja 8 vahel");
            }

            // Kui arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10
            // NB JA on enne VÕI'd, kõige lihtsam on alati sulgu panna.
            if ((a > 2 && a < 8) || (a > 5 && a < 8) || a > 10)
            {
                Console.WriteLine("Kui arv on 2 ja 8 vahel või arv on 5 ja 8 vahel või arv on suurem kui 10");
            }

            Console.ReadLine();

        }
    }
}
