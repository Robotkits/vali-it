﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloName
{
    class Program
    {
        static void Main(string[] args)
        {
            // Deklareerime/defineerime muutuja tüübist string (teksti sisaldav muutuja tüüp) nime "name"
            string name;
            // Anname muutujale "name" väärtuse "Ott"
            name = "Ott";
            name = "Kalle";
            name = "Malle";

            // Escape \"laseb kasutada stringi sees " sümbolit
            // Escape \\ laseb kasutada stringi sees \ sümbolit

            string greeting = "Tere, " + name + "!";

            Console.WriteLine(greeting);

            Console.WriteLine();

            Console.WriteLine("Mis on sinu nimi?");

            name = Console.ReadLine();

            greeting = "Tere, " + name + "!";

            Console.WriteLine(greeting);

            Console.ReadLine();

            Console.WriteLine("Mis on sinu eesnimi?");
            string firstName = Console.ReadLine();

            Console.WriteLine("Mis on sinu perekonnanimi?");
            string lastName = Console.ReadLine();

            greeting = "Tere, " + firstName + " " + lastName + "!";
            Console.WriteLine(greeting);

            Console.ReadLine();

        }
    }
}
