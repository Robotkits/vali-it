﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhileLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            // Lõpmatu while tsükkel
            //while (true)
            //{
            //    Console.WriteLine("Hello");
            //}

            // juhusliku arvu generaatori objekti loomine


            bool isGameOver = false;

            while (!isGameOver)
            {
                Random random = new Random();

                // arvude 1, 2, 3, 4, 5


                int correctnumber = random.Next(1, 11);
                int number = 0;
                bool isFirstTry = true;

                // kui kasutaja pakub midagi muud, kui 1 ja 10 vahel
                // kirjuta kasutajale. proovi uuesti, sest pakutud number ei olnud 1 ja 5 vahel


                // Täienda programmi nii, et õige numbri arvamisel, küsib kasutajalt kas ta tahab
                // uuesti mängu mängida. kui tahab, mõtleb uue numbri ja laseb uuesti arvata.


                while (number != correctnumber)
                {
                    if (!isFirstTry && (number < 1 || number > 10))
                    {
                        Console.WriteLine(" Proovi uuesti, number ei olnud 1 ja 10. vahel.");
                    }
                    else
                    {
                        Console.WriteLine("Arva ära number 1 ja 10 vahel");
                        isFirstTry = false;
                    }

                    number = Convert.ToInt32(Console.ReadLine());

                }
                Console.WriteLine("Õige number oli {0}", number);
                Console.WriteLine("Kas soovid uuesti arvata?");
                Console.WriteLine("Vasta ja/ei");

                string answer = Console.ReadLine();
                if(answer.ToLower() != "ja")
                {
                    isGameOver = true;
                }

            }

        }
    }
}
