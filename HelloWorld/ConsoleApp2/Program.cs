﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Interface on liides, mille läbi üks komponent suhtleb teise komponendiga

// Interface määrab ära public meetodid ja propertid, mis peavad seda kasutavas ( implement) klassis
// olemas olema
// Sama nimega, samade parameetritega ja sama tagastusväärtusega. Sisu otsustab iga klass ise.

namespace Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            var plane = new Plane();
            plane.Fly();

            Console.WriteLine();

            var bird = new Bird();
            bird.Fly();

            Console.WriteLine();

            plane.Drive();
            plane.StopDriving();

            Console.WriteLine();

            var car = new Car();
            car.Drive();
            car.StopDriving(100);

            Console.WriteLine();

            // Interface läbi saab komponente omavahel lihtsasti asendada
            ICanDrive drivingVehicle = car;
            drivingVehicle.Drive();

            drivingVehicle = plane;
            drivingVehicle.Drive();

            // interface on nõuab vähem ressurssi kui boxing/unboxing protsess

            List<ICanFly> flyingObjects = new List<ICanFly>();

            flyingObjects.Add(bird);
            flyingObjects.Add(plane);
            flyingObjects.Add(new Bird());
            flyingObjects.Add(new Plane());

            // Pane kõik listis olevad objektid lendama

            foreach (var flyingObject in flyingObjects)
            {
                flyingObject.Fly();
            }
            Console.WriteLine();

            List<IBirdOrCar> birdOrCars = new List<IBirdOrCar>();
            birdOrCars.Add(new Bird());
            birdOrCars.Add(new Car());

            foreach (var birdOrCar in birdOrCars)
            {
                if (birdOrCar is Car)
                {
                    ((Car)birdOrCar).Drive();
                }
                else if (birdOrCar is Bird)
                {
                    ((Bird)birdOrCar).Fly();
                }
            }

            Console.ReadLine();
        }
    }
}
