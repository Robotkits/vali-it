﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    // I sümboliseerib Interface, ehk can fly, mitte i can fly
    interface ICanFly
    {
        // Määran ära, et igas klassis, mis seda interface implemnteerib
        // peab olema meetod Fly/(, mis ei tagasta midagi
        // ehk void Fly()
        void Fly();
    }
}
