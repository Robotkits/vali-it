﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    class Car : Vehicle, ICanDrive, IBirdOrCar
    {
        public int MaxDistance { get; set; }
        public void Drive()
        {
            Console.WriteLine("Driving");
        }
        public void StopDriving(int afterDistance = 0)
        {
            if (afterDistance != 0)
            {
                Console.WriteLine("Driving stopped after {0}", afterDistance);

            }
            else
            {
                Console.WriteLine("Driving stopped");
            }
            Console.ReadLine();
        }
    }
}
