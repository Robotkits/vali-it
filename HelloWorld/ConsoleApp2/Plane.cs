﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    class Plane : Vehicle, ICanFly, ICanDrive
    {
        public int MaxDistance { get; set; }
        public void Drive()
        {
            Console.WriteLine("Taxiing");
        }
        public void StopDriving(int afterDistance = 0)
        {
            if (afterDistance != 0)
            {
                Console.WriteLine("Taxiing stopped after {0}", afterDistance);

            }
            else
            {
                Console.WriteLine("Taxiing stopped");
            }
        }
        public void Fly()
        {
            DoCheckList();
            StartEngine();
            Console.WriteLine("Plane is flying");
        }

        private void DoCheckList()
        {
            Console.WriteLine("Checklist done!");
        }
        private void StartEngine()
        {
            Console.WriteLine("Engine has started!");
        }
    }
}
