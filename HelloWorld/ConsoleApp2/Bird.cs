﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    class Bird : ICanFly, IBirdOrCar
    {
        public void Fly()
        {
            JumpUp();
            Console.WriteLine("Bird is flying!");
        }
        public void LayEggs()
        {
            Console.WriteLine("Bird is laying eggs");
        }
        private void JumpUp()
        {
            Console.WriteLine("Bird jumped up!");
        }
    }
}
