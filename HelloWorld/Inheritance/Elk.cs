﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Elk : Carnivore
    {
        // Viimane kütitud loom, alati kirjutab viimase kütitud looma
        public bool BullsCows { get; set; }
        public string LastHuntedAnimal { get; private set; }
        public void Hunt(string animal)
        {
            LastHuntedAnimal = animal;
            Console.WriteLine($"Hunting {animal}");
        }
    }
}
