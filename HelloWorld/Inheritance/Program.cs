﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// Pärinemine
// Põlvnemine
namespace Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat persian = new Cat
            {
                Age = 2,
                Breed = "Persian",
                Name = "Garfield",
                Color = "Orange",
                LivesRemaining = 10
            };

            persian.CatchMouse();
            persian.Eat();

            persian.PrintInfo();

            Console.WriteLine();

            Cat angora = new Cat()
            {
                Age = 3,
                Breed = "Angora",
                Name = "Atomic Kitten",
                Color = "Space colors",
            };
            angora.FavoriteToy = "Lendava kalad";
            angora.PrintInfo();
            Console.WriteLine();

            Dog buldog = new Dog()
            {
                Age = 21,
                Breed = "Bulldog",
                Name = "Chuck Norris",
                Color = "White/brown",
                IsChained = true
            };

            buldog.Bark("ru");
            buldog.Eat();
            buldog.PrintInfo();

            Console.WriteLine();
            Elk elk = new Elk();
            elk.Hunt("Rabbit");

            Console.WriteLine($"Last hunted animal was {elk.LastHuntedAnimal}");
            Console.WriteLine();

            Rabbit rabbit = new Rabbit
            {
                Name = "Juta"
            };

            Lion lion = new Lion();
            lion.Hunt(rabbit);

            Wolf wolf = new Wolf();
            wolf.Hunt(rabbit);

            Console.WriteLine("Last hunted animal name {0}", lion.LastHuntedAnimal.Name);
            Console.WriteLine("Last hunted animal name {0}", wolf.LastHuntedAnimal.Name);
            Console.WriteLine();

            Console.WriteLine(wolf);
            Console.WriteLine(angora);
            Console.WriteLine();

            wolf.Eat();
            rabbit.Eat();
            angora.Eat();

            Console.WriteLine();
            angora.Name = "Pille";

            angora.PrintInfo();


            Console.ReadLine();
        }
    }
}
