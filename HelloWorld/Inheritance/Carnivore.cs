﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Carnivore : WildAnimal
    {
        public Animal LastHuntedAnimal { get; private set; }
        
        public void Hunt(Animal animal)
        {
            LastHuntedAnimal = animal;
            Console.WriteLine("Hunting{0}", animal.GetType().Name);
        }

        // Selleks, et pärinevast klassist (WildAnimal) kirjutada üle mingi vanemklassi meetod,
        // tuleb teha 2 sammu:
        // 1. Vanemklassi meetodile tuleb ette kirjutada märksõna virtual
        // mis ütleb, et seda meetodit saavad kõik pärinevad klassid
        // oma nägemuse järgi üle kirjutada.
        // 2. pärinevas klassis meetodi ette, mida tahad üle kirjutada, tuleb
        // kirjutada märksõna override
        public override string ToString()
        {
            return GetInfo();
        }

        public override void Eat()
        {
                Console.WriteLine("Söön loomi");
        }

    }
}
