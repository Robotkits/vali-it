﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class DomesticAnimal : Animal
    {
        public bool UsefulCute{get;set;}
        public string FavoriteToy { get; set; }
        public override void PrintInfo()
        {
            base.PrintInfo();
            if (FavoriteToy != null)
            {
                Console.WriteLine($"Lemmikmänguasi on {FavoriteToy}");
            }
        }
    }
}
