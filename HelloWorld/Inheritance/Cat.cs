﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Cat : Pet
    {
        public int LivesRemaining { get; set; }

        public override string Name
        {
            get
            {
                return "dr. " + base.Name;
            }
        }

        public void CatchMouse()
        {
            Console.WriteLine("Püüan hiiri");
        }

        public override void PrintInfo()
        {
            base.PrintInfo();
            if (LivesRemaining != 0)
            {
                Console.WriteLine($"Elusi on järgi {LivesRemaining}");
            }
        }
    }
}
