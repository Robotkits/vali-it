﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Pig : FarmAnimal
    {
        public override void Eat()
        {
            base.Eat();
        }
        public override void PrintInfo()
        {
            base.PrintInfo();
        }
    }
}
