﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Inheritance
{
    public class Dog : Pet
    {
        private int yearOfBirth;

        public bool IsChained { get; set; }

        // Parameetriteta konstruktor
        // Vaikimisi on alati igal klassil üks tühi parameetriteta konstruktor
        // senikaua kuni me uue konstruktori loome.
        // kui ma loon näiteks uue 2 parameetriga konstruktori, siis kustutakse  vaikimisi nähtamatu parameetritega konstruktor ära
        public Dog()
        {
            File.AppendAllText("dog.txt", "Loodi koer:" + Name + " " + DateTime.Now + "\r\n");
            Console.WriteLine("Loodi Dog objekt");
            // Vaikimisi kõik koerad on Matid
            Name = "Mati";

            yearOfBirth = DateTime.Now.Year;

            // Kõig Dogid on 1 aastased.
            Age = 1;
            // vaikimisi kõik koerad on ketis
            IsChained = true;
        }
        public Dog(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public void Bark(string language)
        {
            if (language == "ru")
            {
                Console.WriteLine("gaf gaf");
            }
            else if(language == "en")
            {
                Console.WriteLine("Woff!! Woff!!");
            }
            else
            {
                Console.WriteLine("Auh Auh");
            }
        }
    }
}
