﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Animal : Object
        // Kõigi objekti klasside ülim madalaim ühik on Object
    {
        public string Breed { get; set; }
        public virtual string Name { get; set; }
        public string Color { get; set; }
        public int Age { get; set; }

        // virtual tähendab, et seda meetodit saavad pärinevad klassid üle kirjutada
        public virtual void Eat()
        {
            Console.WriteLine("söön toitu");
        }
        public virtual void PrintInfo()
        {
            var type = GetType();
            Console.WriteLine($"Technical information:");
            Console.WriteLine($"Name is {Name}");
            Console.WriteLine($"Breed is {Breed}");
            Console.WriteLine($"Color is {Color}");
            Console.WriteLine($"Age is {Age}");
        }
        public string GetInfo()
        {
            return String.Format($"Breed on {Breed}, name is {GetType().Name}. ");
        }
    }
}
