﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    public class Pet : DomesticAnimal
    {
        public string FavoriteToy { set; get; }
        public override void PrintInfo()
        {
            base.PrintInfo();
            if(FavoriteToy != null)
            {
                Console.WriteLine($"Lemmik mänguasi on {FavoriteToy}");
            }
        }
        public bool OutdoorIndoor { get; set; }
    }
}
