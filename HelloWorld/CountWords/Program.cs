﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountWords
{
    class Program
    {
        static void Main(string[] args)
        {
            // Küsi kasutajalt lause
            // ja loe kokku kõik erinevad sõnad ja prindi välja
            // mitu korda need sõnad esinevad

            // "Mina ja Pets läksime tööle ja siis koju ja siis tööle jälle"
            // Mina x1
            // ja x3
            // pets x1
            // läksime x1
            // siis x2
            //koju x1
            // tööle x2
            // jälle x1

            Dictionary<string, int> wordCounts = new Dictionary<string, int>();

            Console.WriteLine("Sisesta lause");
            string sentence = Console.ReadLine();

            string[] words = sentence.Split(' ');

            for (int i = 0; i < words.Length; i++)
            {
                string word = words[i];
                if (!wordCounts.ContainsKey(word))
                {
                    wordCounts.Add(word, 1);
                }
                else
                {
                    //wordCounts[word] = wordCounts[word] + 1;
                    wordCounts[word]++;
                }

            }
            foreach (var item in wordCounts)
            {
                Console.WriteLine($"{item.Key} {item.Value}");
            }
            Console.ReadLine();

            string Word;
            Console.WriteLine("Enter the word!..");
            Word = Console.ReadLine();   // Read the Input string from User at Run Time  
            var Value = Word.Split(' ');  // Split the string using 'Space' and stored it an var variable  
            Dictionary<string, int> RepeatedWordCount = new Dictionary<string, int>();
            for (int i = 0; i < Value.Length; i++) //loop the splited string  
            {
                if (RepeatedWordCount.ContainsKey(Value[i])) // Check if word already exist in dictionary update the count  
                {
                    int value = RepeatedWordCount[Value[i]];
                    RepeatedWordCount[Value[i]] = value + 1;
                }
                else
                {
                    RepeatedWordCount.Add(Value[i], 1);  // if a string is repeated and not added in dictionary , here we are adding   
                }
            }
            Console.WriteLine();
            Console.WriteLine("------------------------------------");
            Console.WriteLine("Repeated words and counts");
            foreach (KeyValuePair<string, int> kvp in RepeatedWordCount)
            {
                Console.WriteLine(kvp.Key + " Counts are " + kvp.Value);  // Print the Repeated word and its count  
            }
            Console.ReadKey();


            Console.ReadLine();
        }

        
    }
}
