﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            // "Auto" => "Car"
            // "Maja" => "House"
            // "Kass" => "Cat"
            // "Koer" => "Dog"

            Dictionary<string, string> dictionaryEN = new Dictionary<string, string>();
            dictionaryEN.Add("auto", "car");
            dictionaryEN.Add("maja", "house");
            dictionaryEN.Add("kass", "cat");
            dictionaryEN.Add("koer", "dog");


            Console.WriteLine("Keys:");
            foreach (string item in dictionaryEN.Keys)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            Console.WriteLine("Value:");
            foreach (string item in dictionaryEN.Values)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();

            //Console.WriteLine("Maja inglise keeles on {0}", dictionaryEN["Maja"]);

            Dictionary<string, string> dictionaryDE = new Dictionary<string, string>();
            dictionaryDE.Add("auto", "auto");
            dictionaryDE.Add("maja", "haus");
            dictionaryDE.Add("kass", "katze");
            dictionaryDE.Add("koer", "hund");

            Console.WriteLine("Mis keelde soovid sõna tõlkida? EN/DE");
            string language = Console.ReadLine();

            Console.WriteLine("Mis sõnu soovi tõlkida?");
            Console.WriteLine($" Tunnen selliseid sõnu : {string.Join(" ", dictionaryEN.Keys)}");

            string word = Console.ReadLine().ToLower();

            if (language == "en")
            {
                Console.WriteLine("See sõna {0} {1} keeles on {2}", word, language, dictionaryEN[word]);
            }
            else
            {
                Console.WriteLine("See sõna {0} {1}keeles on {2}", word, language, dictionaryDE[word]);
            }

            Console.WriteLine();

            // peaks välja võtma kolmanda sõna 
            int counter = 0;
            //string thirdKey = " ";

            foreach (string item in dictionaryEN.Keys)
            {
                Console.WriteLine(item);
                counter++;
                    if (counter == 3)
                {
                    Console.WriteLine(item);
                }
            }

            // Selle tabeliga oleks võimalik küsida keelt, lühendiga või täispika nimega.

            Dictionary<string, string> countryCodes = new Dictionary<string, string>();
            countryCodes.Add("EST", "Estonia");
            countryCodes.Add("EE", "Estonia");
            countryCodes.Add("USA", "United States Of America");
            countryCodes.Add("GER", "Germany");

            // Asenda "United States Of America" => "United States"
            countryCodes["USA"] = "United states";

            // Küsi kasutajalt riigikood ja sellele vastav riigi nimetus
            // Kui selline riigikood on olemas, siis asenda riik
            // Aga kui sellist riigikoodi veel ei olnud, siis lisa

            Console.WriteLine("Kirjutage palun riigikood");
            string code = Console.ReadLine();

            string newCountry = Console.ReadLine();

            if (countryCodes.ContainsKey(newCountry))
            {
                countryCodes[newCountry] = newCountry; 
            }
            else
            {
                countryCodes.Add(code, newCountry);
            }

            //Console.WriteLine($"Sellised on hetkel riigikoodid {string Join.countryCodes.Key} {string Join. countryCodesValude}");

            foreach (var item in countryCodes)
            {
                Console.WriteLine("{0} => {1}", item.Key, item.Value);
            }

            "Peeter".Split(new string[] { ", " }, StringSplitOptions.None);

            Console.ReadLine();
        }
    }
}
