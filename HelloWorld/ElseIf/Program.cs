﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElseIf
{
    class Program
    {
        static void Main(string[] args)
        {
            // küsikasutajalt arv1
            // küsi kasutajalt arv2
            // kaks arvu on võrdsed, prindi tekst: arvud on võrdsed
            // kui esimene arv on suurem kui teine,prindi,et esimene on suurem
            //muuljuhul prindi, et teine on suurem

            // else kehtib alati endale eelneva if kohta

            Console.WriteLine("Palun sisestage kaks arvu, esimesele reale esimene arv ja ENTER, teisele reale teine arv ja ENTER!");
            

            int FirstNumber = Convert.ToInt32(Console.ReadLine());
            int SecondNumber = Convert.ToInt32(Console.ReadLine());

            // olukord, mõlemad arvud on 0 ja 0
            // kujuta samaskeem else if skeemiga 

            if (FirstNumber == 0 && SecondNumber ==0)
            {
                Console.WriteLine("Mõlemad arvud on 0!");
            }
            else if (FirstNumber != 0 && SecondNumber !=0 && FirstNumber == SecondNumber)
            {
                Console.WriteLine("Antud arvud on võrdsed!");
            }
            else

            // Järgnev skeem sai tõstetud else sisse, et välistada väiksema või suurema lisaks kuvamist, kui arvud on võrdsed

            {
                if (FirstNumber < SecondNumber)
                {
                    Console.WriteLine("Esimene arv on väiksem!");
                }
                else
                {
                    Console.WriteLine("Teine arv on väiksem!");
                }
            }

            
            
            Console.ReadLine();
        }

    }
}
