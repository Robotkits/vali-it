﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleVerification
{
    using ForgetTheMilk.Controllers;
    using NUnit.Framework;

    class CreateTaskTests : AssertionHelper
    {
        [Test]
        public void TestDescriptionAndNoDueDate()
        {
            var input = "Pickup the groceries";

            var task = new Task(input, default(DateTime));

            Expect(task.Description, Is.EqualTo(input));
            Assert.AreEqual(null, task.DueDate);
        }

        [Test]
        [TestCase("Pickup the groceries mai 5 - as of 2015-05-31")]
        [TestCase("Pickup the groceries apr 5 - as of 2015-05-31")]
        public void MayDueDateDoesWrapYear(string input)
        {
            var today = new DateTime(2015, 5, 31);

            var task = new Task(input, today);

            Expect(task.DueDate.Value.Year, Is.EqualTo(2016));
        }

        [Test]
        public void MayDueDateDoesNotWrapYear()
        {
            var input = "Pickup the groceries mai 5 - as of 2015-05-04";
            var today = new DateTime(2015, 5, 4);

            var task = new Task(input, today);

            Expect(task.DueDate, Is.EqualTo(new DateTime(2015, 5, 5)));
        }

        [Test]
        [TestCase("Groceries jaan 5", 1)]
        [TestCase("Groceries veebr 5", 2)]
        [TestCase("Groceries märts 5", 3)]
        [TestCase("Groceries apr 5", 4)]
        [TestCase("Groceries mai 5", 5)]
        [TestCase("Groceries juuni 5", 6)]
        [TestCase("Groceries juuli 5", 7)]
        [TestCase("Groceries aug 5", 8)]
        [TestCase("Groceries sept 5", 9)]
        [TestCase("Groceries okt 5", 10)]
        [TestCase("Groceries nov 5", 11)]
        [TestCase("Groceries dets 5", 12)]
        public void DueDate(string input, int expectedMonth)
        {
            var today = new DateTime(2015, 5, 31);

            var task = new Task(input, today);

            Expect(task.DueDate, Is.Not.Null);
            Expect(task.DueDate.Value.Month, Is.EqualTo(expectedMonth));
        }

        [Test]
        public void TwoDigitDay_ParseBothDigits()
        {
            var input = "Groceries apr 10";

            var task = new Task(input, default(DateTime));

            Expect(task.DueDate.Value.Day, Is.EqualTo(10));
        }

        [Test]
        public void DayIsPastTheLastDayOfTheMonth_DoesNotParseDueDate()
        {
            var input = "Groceries apr 44";

            var task = new Task(input, default(DateTime));

            Expect(task.DueDate, Is.Null);
        }

        [Test]
        public void AddVeebr29TaskInMarchOfYearBeforeLeapYear_ParsesDueDate()
        {
            var input = "Groceries veebr 29";
            var today = new DateTime(2015, 3, 1);

            var task = new Task(input, today);

            Expect(task.DueDate.Value, Is.EqualTo(new DateTime(2016,2,29)));
        }

    }
    }
