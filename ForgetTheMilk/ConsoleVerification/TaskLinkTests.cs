﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleVerification
{
    using ForgetTheMilk.Controllers;
    using NUnit.Framework;

    public class TaskLinkTests : AssertionHelper
    {
        [Test]
        public void CreatTast_DescriptionWithALink_SetLink()
        {
            var input = "test http://www.google.com";

            var task = new Task(input, default(DateTime));

            Expect(task.Link, Is.EqualTo("http://www.google.com"));
        }

        [Test]
        public void Validate_InvalidUrl_ThrowsException()
        {
            var input = "http://www.google.com";

            Expect(() => new Task(input, default(DateTime), new LinkValidator()),
                Throws.Exception.With.Message.EqualTo("Invalid link " + input));
        }
    }
}
