﻿using System;

namespace Nullables
{
    class Program
    {
        static void Main(string[] args)
        {
            // annab võimaluse eristada olukorda, kas mul lihtüübil on väärtus antud
            // või on vaikeväärtusg
            int? a = 3;
            a = null;
            a = -4;
            a = null;

            bool? attendedSchool = false;

            //attendedSchool = null;
            attendedSchool = true;

            if(attendedSchool.HasValue)
            {
                Console.WriteLine("TEMA VÄÄRTUS on {0}", attendedSchool.Value);
            }
            else
            {
                Console.WriteLine("ON ILMA VÄÄRTUSETA"); ;
            }
            Console.ReadLine();


            
            DateTime? date = null;
        }
    }
}
