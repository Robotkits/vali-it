namespace DatabaseFirst
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Student")]
    public partial class Student
    {
        [Key]
        [Column(Order = 0)]
        public int Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(30)]
        public string FirstName { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(30)]
        public string LastName { get; set; }

        [Key]
        [Column(Order = 3)]
        public byte Age { get; set; }
    }
}
