namespace DatabaseFirst
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Author> Author { get; set; }
        public virtual DbSet<AuthorBook> AuthorBook { get; set; }
        public virtual DbSet<Book> Book { get; set; }
        public virtual DbSet<Loan> Loan { get; set; }
        public virtual DbSet<LoanType> LoanType { get; set; }
        public virtual DbSet<Student> Student { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Author>()
                .Property(e => e.FirstName)
                .IsFixedLength();

            modelBuilder.Entity<Author>()
                .Property(e => e.LastName)
                .IsFixedLength();

            modelBuilder.Entity<Author>()
                .HasOptional(e => e.AuthorBook)
                .WithRequired(e => e.Author);

            modelBuilder.Entity<Book>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Book>()
                .HasMany(e => e.AuthorBook)
                .WithRequired(e => e.Book)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Student>()
                .Property(e => e.FirstName)
                .IsFixedLength();

            modelBuilder.Entity<Student>()
                .Property(e => e.LastName)
                .IsFixedLength();
        }
    }
}
