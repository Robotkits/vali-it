﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main thread started");
            Task.Run(() => DoWork());
            //Thread.Sleep(5000);
            Console.WriteLine("Main thread done");
            Console.ReadLine();
        }

        static async Task DoWork()
        {
            Console.WriteLine("Starting work");
            for (int i = 1; i < 6; i++)
            {
                Thread.Sleep(2000);
                Console.WriteLine(i);
            }
            await Task.Run(() => DoMoreWork());
            //Task.Run(new Action(DoMoreWork));
            Console.WriteLine("WORK DONE");
        }

        static void DoMoreWork()
        {
            Console.WriteLine("Starting MORE work");
            for (int i = -1; i > -6; i--)
            {
                Thread.Sleep(2000);
                Console.WriteLine(i);
            }
            Console.WriteLine("MORE WORK DONE");
        }
    }
}
