﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Threads
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Main thread started");
            Thread threadA = new Thread(DoWork);
            threadA.Start();

            Thread threadB = new Thread(DoWork);
            threadB.Start();

            Console.WriteLine("Main thread continues");
            Console.WriteLine("MAIN THREAD DONE");

            Console.WriteLine("MIS ON SINU NIMI");
            var name = Console.ReadLine();
            Console.WriteLine("TERE {0}", name);

            // Main thread ootab seni kuni teada on lõpetanud
            threadA.Join();
            threadB.Join();

            Console.WriteLine("Main thread done");
            Console.ReadLine();
        }

        static void DoWork()
        {
            Console.WriteLine("Starting work");
            for (int i = 1;i<6;i++)
            {
                Thread.Sleep(2000);
                Console.WriteLine(i);
            }
            Console.WriteLine("WORK DONE");
        }
    }
}
