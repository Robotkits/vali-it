﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WaitAllTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            Task taskA = Task.Run(() => DoWork());
            Task taskB = Task.Run(() => DoMoreWork());
            Task taskC = Task.Run(() => DoWorkAndSum());


            Task.WaitAll(taskA, taskB, taskC);
            Console.WriteLine("ALL task finished");
            Console.ReadLine();
        }

        static async Task DoWorkAndSum()
        {
            int count = await DoWork();
            Console.WriteLine("{0} jobs done", count);
        }

        static async Task<int> DoWork()
        {
            int count = 5;
            Console.WriteLine("Starting work");
            for (int i = 1; i <= count; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine(i);
            }
            await Task.Run(() => DoMoreWork());
            Console.WriteLine("WORK DONE");
            return count;
        }

        static async Task DoMoreWork()
        {
            Console.WriteLine("Starting MORE work");
            for (int i = -1; i > -6; i--)
            {
                Thread.Sleep(1000);
                Console.WriteLine(i);
            }
            await Task.Run(() => DoEvenMoreWork());
            Console.WriteLine("MORE WORK DONE");
        }

        static async Task DoEvenMoreWork()
        {
            Console.WriteLine("Starting EVEN MORE work");
            for (int i = 1; i < 6; i++)
            {
                Thread.Sleep(1000);
                Console.WriteLine("+" + i);
            }
            Console.WriteLine("EVEN MORE WORK DONE");
        }
    }
}
