﻿using System;
using GeeBee;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeeBee
{
    class DeckOfCards : Card
    {
        const int NUM_OF_CARDS = 36; // number of all cards
        private Card[] deck; // array of all playing cards 

        public DeckOfCards()
        {
            deck = new Card[NUM_OF_CARDS];
        }

        public Card[] GetDeck { get { return deck; } } // get current deck

        // create deck of 36 cards: 9 Values each, with 4 suits
        public void SetUpDeck()
        {
            int i = 0;
            foreach (SUIT s in Enum.GetValues(typeof(SUIT)))
            {
                foreach(VALUE v in Enum.GetValues(typeof(VALUE)))
                {
                    deck[i] = new Card { MySuite = s, MyValue = v };
                    i++;
                }
            }

            ShuffleCards();
        }

        // shuffle the deck
        public void ShuffleCards()
        {
            Random rand = new Random();
            Card temp;

            // run the shuffle 1000 times
            for (int shuffletTimes = 0; shuffletTimes < 1000; shuffletTimes++)
            {
                for (int i = 0; i < NUM_OF_CARDS; i++)
                {
                    // swap the cards
                    int secondCardIndex = rand.Next(9);
                    temp = deck[i];
                    deck[i] = deck[secondCardIndex];
                    deck[secondCardIndex] = temp;
                }
            }
        }
    }
}
