﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeeBee
{
    class Card
    {
        public enum SUIT
        {
            HEARTS,
            SPADES,
            DIAMONDS,
            CLUBS
        }

        public enum VALUE
        {
            SIX = 6, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
        }

        // properties
        public SUIT MySuite { get; set; }
        public VALUE MyValue { get; set; }
    }
}
