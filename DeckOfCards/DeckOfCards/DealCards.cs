﻿using GeeBee;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace Geebee
{
    class DealCards : DeckOfCards
    {
        private readonly Card[] playerHand;
        private readonly Card[] secondPlayerHand;
        private readonly Card[] thirdPlayerHand;
        private readonly Card[] sortedPlayerHand;
        private readonly Card[] sortedSecondPlayerHand;
        private readonly Card[] sortedThirdPlayerHand;

        public DealCards()
        {
            playerHand = new Card[12];
            sortedPlayerHand = new Card[12];
            secondPlayerHand = new Card[12];
            sortedSecondPlayerHand = new Card[12];
            thirdPlayerHand = new Card[12];
            sortedThirdPlayerHand = new Card[12];
        }

        public void Deal()
        {
            SetUpDeck(); // create the deck of cards and shuffle them
            GetHand();
            sortCards();
            DisplayCards();
            //evaluateHands();
        }

        public void GetHand()
        {
            // 12 card for the player
            for (int i = 0; i < 12; i++)
                playerHand[i] = GetDeck[i];

            // 12 card for the player
            for (int i = 12; i < 12; i++)
                secondPlayerHand[i - 12] = GetDeck[i];

            // 12 card for the player
            for (int i = 24; i < 12; i++)
                thirdPlayerHand[i - 24] = GetDeck[i];
        }

        public void sortCards()
        {
            var queryPlayer = from hand in playerHand
                              orderby hand.MyValue
                              select hand;
            var querySecondPlayer = from hand in secondPlayerHand
                                    orderby hand.MyValue
                                    select hand;
            var queryThirdPlayer = from hand in thirdPlayerHand
                                   orderby hand.MyValue
                                   select hand;

            var index = 0;
            foreach (var element in queryPlayer.ToList())
            {
                sortedPlayerHand[index] = element;
                index++;
            }
            index = 0;
            foreach (var element in querySecondPlayer.ToList())
            {
                sortedSecondPlayerHand[index] = element;
                index++;
            }
            index = 0;
            foreach (var element in queryThirdPlayer.ToList())
            {
                sortedThirdPlayerHand[index] = element;
                index++;
            }
        }

        public void DisplayCards()
        {
            Console.Clear(); 
            int x = 0; // x position of the cursor, we move it left and right
            int y = 1; // y position of the cursor, we move up and down

            // display player hand
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("PLAYER HAND");
            for (int i = 0; i < 12; i++)
            {
                DrawCards.DrawCardOutline(x, y);
                DrawCards.PumpUpJam(playerHand[i], x, y);
                x++;
            }
            y = 15; // move the row of secondplayer cards below the player cards
            x = 0; // reset x position
            Console.SetCursorPosition(x, y);
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("SECOND PLAYER HAND");
            for (int i = 12; i < 24; i++)
            {
                DrawCards.DrawCardOutline(x, y);
                DrawCards.PumpUpJam(sortedSecondPlayerHand[i - 12], x, y);
                x++;
            }
        }

        //public void evaluateHands()
        //{

        //}

    }
}
