﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AspNetCore.Models;

namespace AspNetCore.Controllers
{
    public class HomeController : Controller
    {
        // ASP.NET kontekstist kontrolleri meetodid on Action'id
        public IActionResult Index()
        {
            return View();
        }

        

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Services()
        {
            int a = 5;
            int b = 8;

            ViewData["Message"] = String.Format("Arvude {0} ja {1} summa on {2}", a, b, a + b);
            ViewData["Date"] = DateTime.Now;
            ViewData["Pi"] = Math.PI;

            return View();
        }

        public IActionResult HelloWorld()
        {
            return Content("Hello world");
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
