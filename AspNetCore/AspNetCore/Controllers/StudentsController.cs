﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCore.Data;
using AspNetCore.Models;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace AspNetCore.Controllers
{
    public class StudentsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        // Context on otsene viide andmebaasile AspNet
        public StudentsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: Students
        public IActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var users = _userManager.Users.Where(x => x.FirstName != "Kalle").ToList();
                users = _context.Users.Where(x => x.FirstName != "Kalle").ToList();

                // Kasutajanime lauri@mail.ee
                ViewData["Name"] = User.Identity.Name;
                string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            }

            // Sel hetkel, kui ma käivitan .ToList()
            // Tehakse tegelikult päring andmebaasi
            // SELECT * FROM Student

            var students = _context.Student
                .Include(x => x.Loans)
                .ThenInclude(x => x.LoanType)
                .OrderBy(x => x.LastName)
                .ThenBy(x => x.FirstName)
                .ToList();


            return View(students);
        }

        // GET: Students/Details/5
        [Authorize(Roles = "Admin,HR")]
        public async Task<IActionResult> Details(int? id)
        {
            

            if (id == null)
            {  
                return NotFound();
            }

            var student = await _context.Student
                .FirstOrDefaultAsync(m => m.Id == id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // GET: Students/Create

        public IActionResult Create()
        {
            if(User.IsInRole("Admin"))
            {
                return View();
            }

            if (User.IsInRole("HR"))
            {
                return RedirectToAction("Index");
            }
            return Content("Ainult admin saab õpilasi lisada");
           
        }

        // POST: Students/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,Age,IsMarried")] Student student)
        {
            if (ModelState.IsValid)
            {
                // Add meetod on uue rea lisamiseks tabelisse
                // INSERT INTO Student
                // (FirstName,LastName,Age)
                // VALUES
                // ('student.FirstName', 'student.LastName', Student.Age)
                _context.Add(student);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(student);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> AddStudent([Bind("Id,FirstName,LastName,Age,IsMarried")] Student student)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Add(student);
                    await _context.SaveChangesAsync();
                }
                catch (Exception)
                {
                    return StatusCode(500, "Sql serveri viga");
                }
              
                return Json(student);
            }
            return StatusCode(500, "Mudeli väärtuste viga");
        }

        public IActionResult HelloName(string name, int count)
        {
            string text = "";

            for (int i = 0; i < count; i++)
            {
                text += "Hello " + name + Environment.NewLine;
            }
            return Content(text);
        }
        
        public IActionResult GetImageFile(int studentId)
        {
            var student = _context.Student.Find(studentId);
            if(student.Picture == null)
            {
                return Content("Pilti pole");
            }
            return File(student.Picture, "image/jpg");
        }

        public IActionResult SumLoans()
        {
            return Content(Convert.ToString(_context.Loan.Sum(x => x.Amount)));
        }


        // GET: Students/Edit/5

        // int?  tähendab, et see on int tüüp, mis lisaks võib olla ka null
        // Nullable
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                // 404 errori lehe näitamine
                return NotFound();
            }
            // Find on nagu FirstOrDefault, aga otsib ainult Primary Key järgi
            // Meil on alati Primary Key Id
            //_context.Student.FirstOrDefault(x => x.Id = id);
            Student student = _context.Student.Find(id);
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        // POST: Students/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //Bind määrab ära propertied, mis tulevad sellesse meetodisse kaasa
        //ülejäänud ignoreeritakse
        public IActionResult Edit(int id, [Bind("Id,FirstName,LastName,Age,IsMarried,IsBlind")] Student student, IFormFile picture)
        {
            if (id != student.Id)
            {
                return NotFound();
            }
            // Kontroll, kas kõik väljad valideerusid
            if (ModelState.IsValid)
            {
                try
                {
                    // Otsi vormis salvestatud Eesnime ja Perekonnanimega kasutaja, ekke id ei ole sama, mis
                    // muudetaval kasutajal ( Kas on olemas teine kasutaja sama ees ja perekonnanimega
                    var existingStudent = _context.Student.Where(x => x.FirstName == student.FirstName
                    && x.LastName == student.LastName && x.Id != student.Id).FirstOrDefault();

                    if (existingStudent != null) 
                    {
                        ModelState.AddModelError("", "Sama nimega kasutaja on juba olemas");
                        return View(student);
                    }
                    // UPDATE Student
                    // SET Firstname = student.FirstName,
                    //      LastName = student.LastName
                    //      Age = student.Age
                    //      .............................
                    // WHERE
                    //      Id = student.Id
                    // Kui Primary Key student.Id = 0
                    // siis tegelikult tehakse hoopis INSERT
                    // Update ei kirjuta veel andmebaasi. Kirjutatakse alles siis, kui kutsun välja
                    // SaveChangesAsync()

                    // Faili üleslaetud pildi salvestamine
                    //FileStream stream = new FileStream("Pictures/" + student.Id + ".jpg", FileMode.Create, FileAccess.Write);
                    //picture.CopyTo(stream);
                    //stream.Close();
                    //MemoryStream stream = new MemoryStream();
                    //picture.CopyTo(stream);

                    //student.Picture = stream.ToArray();
                    //stream.Close();

                    _context.Update(student);
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentExists(student.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        // Viska uuesti see sama exception, 
                        // mis sa kinni püüdsid
                        throw;
                    }
                }

                // Suunab edasi kindla Actionini
                // nameof(Index) annab mulle stringina Index actioni nime
                // ehk siis tagasta "Index"
                return RedirectToAction(nameof(Index));
            }
            // Võta seesama student objekt (uute andmetega) ja täida 
            // sama view uuesti samade andmetega
            // Lisaks tulevad siia valideerimise veateated
            return View(student);
        }



        // GET: Students/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Student
                .FirstOrDefaultAsync(m => m.Id == id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var student = await _context.Student.FindAsync(id);
            _context.Student.Remove(student);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> DeleteStudent(int id)
        {
            var student = await _context.Student.FindAsync(id);
            _context.Student.Remove(student);
            await _context.SaveChangesAsync();
            return Json(id);
        }

        private bool StudentExists(int id)
        {
            _context.Loan.Include(x => x.Student).ThenInclude(x => x.Loans);
            var student = _context.Student.Where(x => x.Id == id).FirstOrDefault();
            if (student == null)
            {
                return false;
            }
            return true;
            //return _context.Student.Any(e => e.Id == id);
        }
    }
}
