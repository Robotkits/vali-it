﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AspNetCore.Data;
using AspNetCore.Models;

namespace AspNetCore.Controllers
{
    public class TeachersController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TeachersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Teachers
        public async Task<IActionResult> Index()
        {
            return View(await _context.Teacher.ToListAsync());
        }

        // GET: Teachers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teacher = await _context.Teacher
                .FirstOrDefaultAsync(m => m.Id == id);
            if (teacher == null)
            {
                return NotFound();
            }

            return View(teacher);
        }

        // GET: Teachers/Create
        public IActionResult Create()
        {
            ViewBag.StudentList =
            new MultiSelectList(_context.Student.Select(x => new SelectListItem()
            {
                Text = x.FullName, Value = x.Id.ToString()
            }),
            "Value",
            "Text");
            return View();
        }

        // POST: Teachers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FullName,SelectedStudentIds")] Teacher teacher)
        {
            if (ModelState.IsValid)
            {
                _context.Add(teacher);
                await _context.SaveChangesAsync();

                if (teacher.SelectedStudentIds != null)
                {
                    foreach (var studentId in teacher.SelectedStudentIds)
                    {
                        var studentTeacher = new StudentTeacher()
                        {
                            StudentId = studentId,
                            TeacherId = teacher.Id
                        };
                        await _context.SaveChangesAsync();
                    }
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }

            }
            return View(teacher);
        }

        // GET: Teachers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teacher = await _context.Teacher.FindAsync(id);
            if (teacher == null)
            {
                return NotFound();
            }

            teacher.SelectedStudentIds = _context.StudentTeacher.Where(x => x.TeacherId == teacher.Id).Select(x => x.StudentId).ToList();

            ViewBag.StudentList =
            new MultiSelectList(_context.Student.Select(x => new SelectListItem()
            {
                Text = x.FullName,
                Value = x.Id.ToString()
            }),
            "Value", 
            "Text");

            return View(teacher);
        }

        // POST: Teachers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FullName, SelectedStudentIds")] Teacher teacher)
        {
            if (id != teacher.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(teacher);

                    var existingStudentIds = _context.StudentTeacher.Where(x => x.TeacherId == teacher.Id).Select(x => x.StudentId).ToList();

                    var studentIdsToAdd = teacher.SelectedStudentIds.Except(existingStudentIds);
                    // Except tagastab esimesest listist need ID'd, mida teises listis ei olnud
                    foreach (var studentId in studentIdsToAdd)
                    {
                        var studentTeacher = new StudentTeacher()
                        {
                            StudentId = studentId,
                            TeacherId = teacher.Id
                        };
                        await _context.SaveChangesAsync();
                    }

                    if (teacher.SelectedStudentIds != null)
                    {
                        var studentIdsToDelete = teacher.SelectedStudentIds.Except(existingStudentIds);
                        // Except tagastab esimesest listist need ID'd, mida teises listis ei olnud
                        foreach (var studentId in studentIdsToDelete)
                        {
                            var studentTeacher = await _context.StudentTeacher.Where(x => x.StudentId == studentId && x.TeacherId == teacher.Id).FirstOrDefaultAsync();
                            _context.Remove(studentTeacher);
                        }
                    }

                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TeacherExists(teacher.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(teacher);
        }

        // GET: Teachers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var teacher = await _context.Teacher
                .FirstOrDefaultAsync(m => m.Id == id);
            if (teacher == null)
            {
                return NotFound();
            }

            return View(teacher);
        }

        // POST: Teachers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var teacher = await _context.Teacher.FindAsync(id);
            _context.Teacher.Remove(teacher);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TeacherExists(int id)
        {
            return _context.Teacher.Any(e => e.Id == id);
        }
    }
}
