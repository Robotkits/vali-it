﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCore.Models
{
    public class Teacher
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public ICollection<StudentTeacher> StudentTeachers {get;set;}

        [NotMapped]
        public ICollection<int> SelectedStudentIds { get; set; }
    }
}
