﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCore.Models
{
    public class Student
    {   [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Eesnimi on kohustulik")]
        [StringLength(10, MinimumLength = 5, ErrorMessage = "Eesnime pikkus peab olema 5 ja 10 vahel")]
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Range(1, 35, ErrorMessage = "Vanus peab olema 1 ja 35 vahel")]
        public int? Age { get; set; }

        [Display(Name = "Is Married")]
        public bool? IsMarried { get; set; }

        [Display(Name = "Is Blind")]
        public bool IsBlind { get; set; }
        public List<Loan> Loans { get; set; }

        [NotMapped] // Attribute 
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        public Guid UserId { get; set; }
        public ApplicationUser User { get; set; }

        public byte[]  Picture { get; set; }

        public virtual ICollection<StudentTeacher> StudentTeachers { get; set; }


    }
}
