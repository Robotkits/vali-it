﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCore.Models
{
    public class Loan
    {
        public int Id { get; set; }
        public double Amount { get; set; }

        [DataType(DataType.Date)]
        public DateTime DueDate { get; set; }
        public LoanType LoanType { get; set; }
        public Student Student { get; set; }
        public int StudentId { get; set; }
        public int LoanTypeId { get; set; }
    }
}
